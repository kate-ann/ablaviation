<?php
/**
* Template Name: Company
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

  <?php if ( get_field( 'x5_page_intro_heading' ) ): ?>

    <?php if ( get_field( 'x5_page_intro_id' ) ): ?>
      <section id="<?php echo esc_attr( get_field ( 'x5_page_intro_id' ) ); ?>" class="c-intro--inner">
    <?php else: ?>
      <section class="c-intro--inner">
    <?php endif; ?>

      <div class="o-container">

        <?php if ( get_field( 'x5_page_intro_heading' ) ): ?>
          <?php echo wp_kses_post( force_balance_tags( get_field( 'x5_page_intro_heading' ) ) ); ?>
        <?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-intro--inner -->

  <?php endif; ?>


  <?php if ( get_field( 'x5_company_about_desc' ) ||
             have_rows( 'x5_company_about_rows' ) ||
             get_field( 'x5_company_about_video_link' ) ||
             get_field( 'x5_company_about_video_pic' ) ): ?>

    <?php if ( get_field( 'x5_company_about_id' ) ): ?>
      <section id="<?php echo esc_attr( get_field ( 'x5_company_about_id' ) ); ?>" class="c-about">
    <?php else: ?>
      <section class="c-about">
    <?php endif; ?>

      <div class="o-container">

        <div class="o-holder">

          <div class="block left">

            <?php if ( get_field( 'x5_company_about_desc' ) ): ?>
              <div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_company_about_desc' ) ) ); ?></div>
            <?php endif; ?>

            <?php if ( have_rows( 'x5_company_about_rows' ) ): ?>

              <div class="btns">

                <?php while( have_rows( 'x5_company_about_rows' ) ) : the_row(); ?>

                  <?php if ( get_sub_field( 'x5_company_about_rows_link' ) ):
                    $x5_company_about_rows_link = get_sub_field( 'x5_company_about_rows_link' ); ?>

                    <?php if ( get_sub_field( 'x5_company_about_rows_type' ) == 'Default' ): ?>

                      <a class="c-btn c-btn--no-fill" href="<?php echo esc_url( $x5_company_about_rows_link['url'] ); ?>" target="<?php echo esc_attr( $x5_company_about_rows_link['target'] ); ?>"><?php echo esc_html( $x5_company_about_rows_link['title'] ); ?></a>

                    <?php else: ?>

                      <a class="c-btn c-btn--no-fill c-btn--alt" href="<?php echo esc_url( $x5_company_about_rows_link['url'] ); ?>" target="<?php echo esc_attr( $x5_company_about_rows_link['target'] ); ?>"><?php echo esc_html( $x5_company_about_rows_link['title'] ); ?></a>

                    <?php endif; ?>

                  <?php endif; ?>

                <?php endwhile; ?>

              </div>
              <!-- btns -->

            <?php endif; ?>

          </div>
          <!-- block -->

          <?php if ( get_field( 'x5_company_about_video_link' ) ||
                     get_field( 'x5_company_about_video_pic' ) ): ?>

            <div class="c-video">

              <?php if ( get_field( 'x5_company_about_video_link' ) ):
                $x5_company_about_video_link = get_field( 'x5_company_about_video_link' ); ?>
                <a class="play-btn open-popup-video" href="<?php echo esc_url( $x5_company_about_video_link ); ?>"><span></span></a>
              <?php endif; ?>

              <?php if ( get_field( 'x5_company_about_video_pic' ) ):
                $x5_company_about_video_pic = get_field( 'x5_company_about_video_pic' );
                $x5_company_about_video_pic_size = wp_get_attachment_image_url( $x5_company_about_video_pic['ID'], 'company_video_cover' ); ?>

                <img src="<?php echo esc_url( $x5_company_about_video_pic_size ); ?>" alt="<?php echo esc_attr( $x5_company_about_video_pic['alt'] ); ?>" />

              <?php endif; ?>

            </div>
            <!-- c-video -->

          <?php endif; ?>

        </div>
        <!-- o-holder -->

      </div>
      <!-- o-container -->
    </section>
    <!-- c-about -->

  <?php endif; ?>


  <?php if ( get_field( 'x5_company_thoughts_heading' ) ||
             get_field( 'x5_company_thoughts_desc' ) ||
             get_field( 'x5_company_thoughts_link' ) ||
             have_rows( 'x5_company_thoughts_rows' )): ?>

    <?php if ( get_field( 'x5_company_thoughts_id' ) ): ?>
      <section id="<?php echo esc_attr( get_field ( 'x5_company_thoughts_id' ) ); ?>" class="c-thoughts c-reports">
    <?php else: ?>
      <section class="c-thoughts c-reports">
    <?php endif; ?>

      <div class="o-container">

        <div class="o-holder">

          <?php if ( have_rows( 'x5_company_thoughts_rows' ) ): ?>

            <div class="c-slider js-slider-pdf">

              <?php while( have_rows( 'x5_company_thoughts_rows' ) ) : the_row(); ?>

                <?php if ( get_sub_field( 'x5_company_thoughts_rows_pic' ) ||
                           get_sub_field( 'x5_company_thoughts_rows_title' ) ||
                           get_sub_field( 'x5_company_thoughts_rows_info' ) ||
                           get_sub_field( 'x5_company_thoughts_rows_file' ) ): ?>

                  <div>
                    <div class="col">

                      <?php if ( get_sub_field( 'x5_company_thoughts_rows_pic' ) ):
                        $x5_company_thoughts_rows_pic = get_sub_field( 'x5_company_thoughts_rows_pic' );
                        $x5_company_thoughts_rows_pic_size = wp_get_attachment_image_url( $x5_company_thoughts_rows_pic['ID'], 'company_document_cover' ); ?>

                        <img src="<?php echo esc_url( $x5_company_thoughts_rows_pic_size ); ?>" alt="<?php echo esc_attr( $x5_company_thoughts_rows_pic['alt'] ); ?>" />

                      <?php endif; ?>

                      <?php if ( get_sub_field( 'x5_company_thoughts_rows_title' ) ): ?>
                        <span class="title"><?php echo esc_html( get_sub_field( 'x5_company_thoughts_rows_title' ) ); ?></span>
                      <?php endif; ?>

                      <?php if ( get_sub_field( 'x5_company_thoughts_rows_info' ) ): ?>
                        <span class="details"><?php echo esc_html( get_sub_field( 'x5_company_thoughts_rows_info' ) ); ?></span>
                      <?php endif; ?>

                      <?php if ( get_sub_field( 'x5_company_thoughts_rows_file' ) ):
                        $x5_company_thoughts_rows_file = get_sub_field( 'x5_company_thoughts_rows_file' ); ?>
                        <a class="c-link" href="<?php echo esc_url( $x5_company_thoughts_rows_file['url'] ); ?>"><span class="icon-download"></span><?php echo esc_html( 'Download' ); ?></a>
                      <?php endif; ?>

                    </div>
                    <!-- col -->
                  </div>

                <?php endif; ?>

              <?php endwhile; ?>

            </div>
            <!-- c-slider -->

          <?php endif; ?>


          <?php if ( get_field( 'x5_company_thoughts_heading' ) ||
                     get_field( 'x5_company_thoughts_desc' ) ||
                     get_field( 'x5_company_thoughts_link' ) ): ?>

            <div class="block right">

              <?php if ( get_field( 'x5_company_thoughts_heading' ) ): ?>
                <?php echo wp_kses_post( force_balance_tags( get_field( 'x5_company_thoughts_heading' ) ) ); ?>
              <?php endif; ?>

              <?php if ( get_field( 'x5_company_thoughts_desc' ) ): ?>
                <div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_company_thoughts_desc' ) ) ); ?></div>
              <?php endif; ?>

              <?php if ( get_field( 'x5_company_thoughts_link' ) ):
                $x5_company_thoughts_link = get_field( 'x5_company_thoughts_link' ); ?>
                <a class="c-btn c-btn--no-fill" href="<?php echo esc_url( $x5_company_thoughts_link['url'] ); ?>" target="<?php echo esc_attr( $x5_company_thoughts_link['target'] ); ?>"><span class="icon-graph"></span><?php echo esc_html( $x5_company_thoughts_link['title'] ); ?></a>
              <?php endif; ?>

            </div>
            <!-- block -->

          <?php endif; ?>

        </div>
        <!-- o-holder -->

      </div>
      <!-- o-container -->
    </section>
    <!-- c-thoughts -->

  <?php endif; ?>


  <?php if ( get_field( 'x5_company_social_news_is' ) ||
             get_field( 'x5_company_social_twitter_is' ) ): ?>

    <?php if ( get_field( 'x5_company_social_id' ) ): ?>
      <section id="<?php echo esc_attr( get_field ( 'x5_company_social_id' ) ); ?>" class="c-news">
    <?php else: ?>
      <section class="c-news">
    <?php endif; ?>

      <div class="o-container">

        <div class="o-holder">

          <?php if ( get_field( 'x5_company_social_news_is' ) ): ?>

            <div class="col news">

              <?php echo wp_kses_post( force_balance_tags( '<h5>LATEST NEWS</h5>' ) ); ?>

              <?php if ( have_rows( 'x5_general_news_rows', 'option' ) ):
                $x5_general_news_row = 1;
                $x5_general_news_rows_total = count( get_field( 'x5_general_news_rows', 'option' ) ); ?>

                <?php while( have_rows( 'x5_general_news_rows', 'option' ) ) : the_row(); ?>

                  <?php if ( $x5_general_news_row > $x5_general_news_rows_total-2 ): ?>

                    <div class="row">

                      <?php if ( get_sub_field( 'x5_general_news_rows_pic', 'option' ) ):
                        $x5_general_news_rows_pic = get_sub_field( 'x5_general_news_rows_pic', 'option' );
                        $x5_general_news_rows_pic_size = wp_get_attachment_image_url( $x5_general_news_rows_pic['ID'], 'company_news_thumb' ); ?>

                        <img class="thumb" src="<?php echo esc_url( $x5_general_news_rows_pic_size ); ?>" alt="<?php echo esc_attr( $x5_general_news_rows_pic['alt'] ); ?>" />

                      <?php endif; ?>

                      <div class="info">

                        <?php if ( get_sub_field( 'x5_general_news_rows_date', 'option' ) ): ?>
                          <span class="date"><?php echo esc_html( get_sub_field( 'x5_general_news_rows_date', 'option' ) ); ?></span>
                        <?php endif; ?>

                        <?php if ( get_sub_field( 'x5_general_news_rows_heading', 'option' ) ): ?>
                          <p class="title"><?php echo esc_html( get_sub_field( 'x5_general_news_rows_heading', 'option' ) ); ?></p>
                        <?php endif; ?>

                        <?php if ( get_sub_field( 'x5_general_news_rows_excerpt_company', 'option' ) ): ?>
                          <div class="desc"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_general_news_rows_excerpt_company', 'option' ) ) ); ?></div>
                        <?php endif; ?>

                        <?php if ( get_sub_field( 'x5_general_news_rows_link', 'option' ) ):
                          $x5_general_news_rows_link = get_sub_field( 'x5_general_news_rows_link', 'option' ); ?>
                          <a class="c-link" href="<?php echo esc_url( $x5_general_news_rows_link['url'] ); ?>" target="<?php echo esc_attr( $x5_general_news_rows_link['target'] ); ?>"><?php echo esc_html( $x5_general_news_rows_link['title'] ); ?></a>
                        <?php endif; ?>

                      </div>
                    </div>
                    <!-- news -->

                  <?php endif; ?>

                  <?php $x5_general_news_row ++; ?>

                <?php endwhile; ?>

              <?php endif; ?>

            </div>
            <!-- col -->

          <?php endif; ?>

          <?php if ( get_field( 'x5_company_social_twitter_is' ) &&
                     get_field( 'x5_company_social_twitter_feed' ) ): ?>

            <div class="col tweets">
              <h5>TWEETS</h5>
              <div class="tweets-holder">
                <?php echo get_field( 'x5_company_social_twitter_feed' ); ?>
              </div>
            </div>
            <!-- col -->

          <?php endif; ?>

        </div>
        <!-- o-holder -->

      </div>
      <!-- o-container -->
    </section>
    <!-- c-news -->

  <?php endif; ?>

  <?php if ( get_field( 'x5_company_global_contact_heading' ) ||
             get_field( 'x5_company_global_contact_desc' ) ||
             get_field( 'x5_company_global_contact_link' ) ||
             get_field( 'x5_company_global_contact_map_is' ) ||
             get_field( 'x5_company_global_contact_social_is' )): ?>

    <?php if ( get_field( 'x5_company_global_contact_id' ) ): ?>
      <section id="<?php echo esc_attr( get_field ( 'x5_company_global_contact_id' ) ); ?>" class="c-contact">
    <?php else: ?>
      <section class="c-contact">
    <?php endif; ?>

      <div class="o-container">

        <div class="o-holder">

          <div class="block left">

            <?php if ( get_field( 'x5_company_global_contact_heading' ) ): ?>
              <?php echo wp_kses_post( force_balance_tags( get_field( 'x5_company_global_contact_heading' ) ) ); ?>
            <?php endif; ?>

            <?php if ( get_field( 'x5_company_global_contact_desc' ) ): ?>
              <div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_company_global_contact_desc' ) ) ); ?></div>
            <?php endif; ?>

            <div class="btns">

              <?php if ( get_field( 'x5_company_global_contact_link' ) ):
                $x5_company_global_contact_link = get_field( 'x5_company_global_contact_link' ); ?>
                <a class="c-btn c-btn--no-fill c-btn--animate" href="<?php echo esc_url( $x5_company_global_contact_link['url'] ); ?>" target="<?php echo esc_attr( $x5_company_global_contact_link['target'] ); ?>"><?php echo esc_html( $x5_company_global_contact_link['title'] ); ?></a>
              <?php endif; ?>

              <?php if ( get_field( 'x5_company_global_contact_social_is' ) ): ?>
                <?php get_template_part( 'partials/social', 'buttons' ); ?>
              <?php endif; ?>

            </div>
            <!-- btns -->

          </div>
          <!-- block -->

          <?php if ( get_field( 'x5_company_global_contact_map_is' ) ): ?>
            <?php get_template_part( 'partials/section', 'map' ); ?>
          <?php endif; ?>

        </div>
        <!-- o-holder -->

      </div>
      <!-- o-container -->
    </section>
    <!-- c-contact -->

  <?php endif; ?>


  <?php if ( have_rows( 'x5_company_contact_rows' ) ||
             get_field( 'x5_company_contact_form_is' ) ): ?>

    <?php if ( get_field( 'x5_company_contact_id' ) ): ?>
      <section id="<?php echo esc_attr( get_field ( 'x5_company_contact_id' ) ); ?>" class="c-contact-form">
    <?php else: ?>
      <section class="c-contact-form">
    <?php endif; ?>

      <div class="o-container">

        <?php if ( have_rows( 'x5_company_contact_rows' ) ): ?>

          <div class="o-holder">

            <?php while( have_rows( 'x5_company_contact_rows' ) ) : the_row(); ?>

              <?php if ( get_sub_field( 'x5_company_contact_rows_pic' ) ||
                         get_sub_field( 'x5_company_contact_rows_heading' ) ): ?>

                <div class="col">

                  <?php if ( get_sub_field( 'x5_company_contact_rows_pic' ) ):
                    $x5_company_contact_rows_pic = get_sub_field( 'x5_company_contact_rows_pic' );?>
                    <img src="<?php echo esc_url( $x5_company_contact_rows_pic['url'] ); ?>" alt="<?php echo esc_attr( $x5_company_contact_rows_pic['alt'] ); ?>" title="<?php echo esc_attr( $x5_company_contact_rows_pic['title'] ); ?>" />
                  <?php endif; ?>

                  <?php if ( get_sub_field( 'x5_company_contact_rows_heading' ) ): ?>
                    <?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_company_contact_rows_heading' ) ) ); ?>
                  <?php endif; ?>

                </div>

              <?php endif; ?>

            <?php endwhile; ?>

          </div>
          <!-- o-holder -->

        <?php endif; ?>

        <?php if ( get_field( 'x5_company_contact_form_is' ) &&
                   get_field( 'x5_company_contact_form_id' ) ): ?>

          <?php gravity_form( esc_html( get_field( 'x5_company_contact_form_id' ) ), false, false, false, '', true, false ); ?>

        <?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-contact-form -->

  <?php endif; ?>


  <?php if ( get_field( 'x5_page_horizons_is' ) ): ?>
    <?php get_template_part( 'partials/section', 'horizons' ); ?>
  <?php endif; ?>

<?php get_footer();
