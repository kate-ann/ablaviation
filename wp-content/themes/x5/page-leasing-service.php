<?php
/**
* Template Name: Leasing Service
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

	<?php if ( get_field( 'x5_page_intro_heading' ) ): ?>

		<?php if ( get_field( 'x5_page_intro_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_page_intro_id' ) ); ?>" class="c-intro--inner">
		<?php else: ?>
			<section class="c-intro--inner">
		<?php endif; ?>

      <div class="o-container">

      	<?php if ( get_field( 'x5_page_intro_heading' ) ): ?>
      		<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_page_intro_heading' ) ) ); ?>
      	<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-intro--inner -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_leasing_assets_heading' ) ||
						 get_field( 'x5_leasing_assets_desc' ) ||
						 get_field( 'x5_leasing_assets_map_is' ) ): ?>

		<?php if ( get_field( 'x5_leasing_assets_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_leasing_assets_id' ) ); ?>" class="c-assets">
		<?php else: ?>
			<section class="c-assets">
		<?php endif; ?>

      <div class="o-container">

				<?php if ( get_field( 'x5_leasing_assets_heading' ) ||
									 get_field( 'x5_leasing_assets_desc' ) ): ?>

					<div class="block left">

	          <?php if ( get_field( 'x5_leasing_assets_heading' ) ): ?>
	          	<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_leasing_assets_heading' ) ) ); ?>
	          <?php endif; ?>

	          <?php if ( get_field( 'x5_leasing_assets_desc' ) ): ?>
	          	<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_leasing_assets_desc' ) ) ); ?></div>
	          <?php endif; ?>

	        </div>
	        <!-- block right -->

				<?php endif; ?>

        <?php if ( get_field( 'x5_leasing_assets_map_is' ) ): ?>
        	<?php get_template_part( 'partials/section', 'map' ); ?>
        <?php endif; ?>

      </div>
      <!-- o-container -->

    </section>
    <!-- c-contact -->

	<?php endif; ?>

	<?php if ( get_field( 'x5_leasing_services_heading' ) ||
						 have_rows( 'x5_leasing_services_rows' ) ): ?>

		<?php if ( get_field( 'x5_leasing_services_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_leasing_services_id' ) ); ?>" class="c-points c-points--quadro c-services">
		<?php else: ?>
			<section class="c-points c-points--quadro c-services">
		<?php endif; ?>

      <div class="o-container">

				<?php if ( get_field( 'x5_leasing_services_heading' ) ): ?>
					<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_leasing_services_heading' ) ) ); ?>
				<?php endif; ?>

        <?php if ( have_rows( 'x5_leasing_services_rows' ) ): ?>

        	<div class="o-holder">

        		<?php while( have_rows( 'x5_leasing_services_rows' ) ) : the_row(); ?>

        			<?php if ( get_sub_field( 'x5_leasing_services_rows_heading' ) ): ?>
        				<div class="col"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_leasing_services_rows_heading' ) ) ); ?></div>
        			<?php endif; ?>

        		<?php endwhile; ?>

        	</div>
        	<!-- o-holder -->
 			<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-services -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_leasing_solutions_heading' ) ||
						 get_field( 'x5_leasing_solutions_desc' ) ||
						 have_rows( 'x5_leasing_solutions_rows' ) ): ?>

		<?php if ( get_field( 'x5_leasing_solutions_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_leasing_solutions_id' ) ); ?>" class="c-points c-points--quadro c-solutions">
		<?php else: ?>
			<section class="c-points c-points--quadro c-solutions">
		<?php endif; ?>

      <div class="o-container">

				<?php if ( get_field( 'x5_leasing_solutions_heading' ) ): ?>
					<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_leasing_solutions_heading' ) ) ); ?>
				<?php endif; ?>

        <?php if ( get_field( 'x5_leasing_solutions_desc' ) ): ?>
        	<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_leasing_solutions_desc' ) ) ); ?></div>
        <?php endif; ?>

				<?php if ( have_rows( 'x5_leasing_solutions_rows' ) ): ?>

					<div class="o-holder">

						<?php while( have_rows( 'x5_leasing_solutions_rows' ) ) : the_row(); ?>

							<?php if ( get_sub_field( 'x5_leasing_solutions_rows_heading' ) ): ?>
								<div class="col"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_leasing_solutions_rows_heading' ) ) ); ?></div>
							<?php endif; ?>

						<?php endwhile; ?>

					</div>
        	<!-- o-holder -->

				<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-services -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_page_horizons_is' ) ): ?>
		<?php get_template_part( 'partials/section', 'horizons' ); ?>
	<?php endif; ?>

<?php get_footer();
