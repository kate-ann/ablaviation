<?php
/**
* Template Name: Media
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

	<?php if ( get_field( 'x5_page_intro_heading' ) ): ?>

		<?php if ( get_field( 'x5_page_intro_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_page_intro_id' ) ); ?>" class="c-intro--inner">
		<?php else: ?>
			<section class="c-intro--inner">
		<?php endif; ?>

      <div class="o-container">

      	<?php if ( get_field( 'x5_page_intro_heading' ) ): ?>
      		<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_page_intro_heading' ) ) ); ?>
      	<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-intro--inner -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_media_reports_heading' ) ||
						 get_field( 'x5_media_reports_desc' ) ||
						 have_rows( 'x5_media_reports_rows' ) ): ?>

		<?php if ( get_field( 'x5_media_reports_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_media_reports_id' ) ); ?>" class="c-thoughts">
		<?php else: ?>
			<section class="c-thoughts">
		<?php endif; ?>

      <div class="o-container">

				<?php if ( get_field( 'x5_media_reports_heading' ) ): ?>
					<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_media_reports_heading' ) ) ); ?>
				<?php endif; ?>

        <?php if ( get_field( 'x5_media_reports_desc' ) ): ?>
        	<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_media_reports_desc' ) ) ); ?></div>
        <?php endif; ?>

				<?php if ( have_rows( 'x5_media_reports_rows' ) ):
					$x5_media_reports_row = 1;
					$x5_media_reports_total = count( get_field( 'x5_media_reports_rows' ) ); ?>

					<div class="js-slider-thoughts c-slider-inner-dots">

						<?php while( have_rows( 'x5_media_reports_rows' ) ) : the_row(); ?>

							<?php if ( $x5_media_reports_row == 1 || $x5_media_reports_row % 5 == 0 ) : ?>

								<div>
            			<div class="o-holder">

							<?php endif; ?>

								<div class="col">

									<?php if ( get_sub_field( 'x5_media_reports_rows_pic' ) ):
										$x5_media_reports_rows_pic = get_sub_field( 'x5_media_reports_rows_pic' );
										$x5_media_reports_rows_size = wp_get_attachment_image_url( $x5_media_reports_rows_pic['ID'], 'media_report_thumb' ); ?>

										<img src="<?php echo esc_url( $x5_media_reports_rows_size ); ?>" alt="<?php echo esc_attr( $x5_media_reports_rows_pic['alt'] ); ?>" />

									<?php endif; ?>

	                <div class="info">

	                	<?php if ( get_sub_field( 'x5_media_reports_rows_date' ) ): ?>
	                		<span class="date"><?php echo esc_html( get_sub_field( 'x5_media_reports_rows_date' ) ); ?></span>
	                	<?php endif; ?>

	                  <?php if ( get_sub_field( 'x5_media_reports_rows_heading' ) ): ?>
	                  	<h4><?php echo esc_html( get_sub_field( 'x5_media_reports_rows_heading' ) ); ?></h4>
	                  <?php endif; ?>

	                  <?php if ( get_sub_field( 'x5_media_reports_rows_desc' ) ): ?>
	                  	<div class="excerpt"><p><?php echo esc_html( get_sub_field( 'x5_media_reports_rows_desc' ) ); ?></p></div>
	                  <?php endif; ?>

										<?php if ( get_sub_field( 'x5_media_reports_rows_link' ) ):
											$x5_media_reports_rows_link = get_sub_field( 'x5_media_reports_rows_link' ); ?>
											<a href="<?php echo esc_url( $x5_media_reports_rows_link['url'] ); ?>" class="c-link" target="<?php echo esc_attr( $x5_media_reports_rows_link['target'] ); ?>"><?php echo esc_html( 'Read More' ); ?></a>
										<?php endif; ?>

	                </div>
	              </div>
	              <!-- col -->

	              <?php if ( $x5_media_reports_row % 4 == 0 || $x5_media_reports_row == $x5_media_reports_total ) : ?>

										</div>
										<!-- slide -->
            			</div>
            			<!-- o-holder -->

								<?php endif; ?>

							<?php $x5_media_reports_row ++; ?>
						<?php endwhile; ?>

					</div>

				<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-thoughts -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_media_news_heading' ) ||
						 get_field( 'x5_media_news_link' ) ): ?>

		<?php if ( get_field( 'x5_media_news_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_media_news_id' ) ); ?>" class="c-news">
		<?php else: ?>
			<section class="c-news">
		<?php endif; ?>

      <div class="o-container">

				<?php if ( get_field( 'x5_media_news_heading' ) ): ?>
					<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_media_news_heading' ) ) ); ?>
				<?php endif; ?>

				<?php if ( have_rows( 'x5_general_news_rows', 'option' ) ):
					$x5_general_news_row = 1; ?>

					<div class="c-slider js-slider-news c-slider-inner-dots">

						<?php while( have_rows( 'x5_general_news_rows', 'option' ) ) : the_row(); ?>

							<div>
		            <div class="col">

		            	<?php if ( get_sub_field( 'x5_general_news_rows_pic', 'option' ) ):
		            		$x5_general_news_rows_pic = get_sub_field( 'x5_general_news_rows_pic', 'option' );
		            		$x5_general_news_rows_pic_size = wp_get_attachment_image_url( $x5_general_news_rows_pic['ID'], 'media_news_thumb' ); ?>

		            		<img src="<?php echo esc_url( $x5_general_news_rows_pic_size ); ?>" alt="<?php echo esc_attr( $x5_general_news_rows_pic['alt'] ); ?>" />

		            	<?php endif; ?>

		              <?php if ( get_sub_field( 'x5_general_news_rows_date', 'option' ) ): ?>
		              	<span class="date"><?php echo esc_html( get_sub_field( 'x5_general_news_rows_date', 'option' ) ); ?></span>
		              <?php endif; ?>


		              <div class="info">
		              	<?php if ( get_sub_field( 'x5_general_news_rows_heading', 'option' ) ): ?>
		              		<h5><?php echo esc_html( get_sub_field( 'x5_general_news_rows_heading', 'option' ) ); ?></h5>
		              	<?php endif; ?>

		                <span class="c-link open-popup-news" href="#news-0<?php echo $x5_general_news_row; ?>"><?php echo esc_html( 'Read more' ); ?></span>
		              </div>
		              <!-- info -->

		              <div id="news-0<?php echo $x5_general_news_row; ?>" class="popup-content mfp-hide">

		              	<?php if ( get_sub_field( 'x5_general_news_rows_date', 'option' ) ): ?>
		              		<span class="date"><?php echo esc_html( get_sub_field( 'x5_general_news_rows_date', 'option' ) ); ?></span>
		              	<?php endif; ?>

		                <?php if ( get_sub_field( 'x5_general_news_rows_heading', 'option' ) ): ?>
		              		<h5><?php echo esc_html( get_sub_field( 'x5_general_news_rows_heading', 'option' ) ); ?></h5>
		              	<?php endif; ?>

										<?php if ( get_sub_field( 'x5_general_news_rows_full_text', 'option' ) ): ?>
											<?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_general_news_rows_full_text' , 'option') ) ); ?>
										<?php endif; ?>

		              </div>
		              <!-- popup-content -->

		            </div>
		            <!-- col -->
		          </div>

		          <?php $x5_general_news_row ++; ?>

						<?php endwhile; ?>

					</div>
        	<!-- c-slider -->

				<?php endif; ?>


				<?php if ( get_field( 'x5_media_news_link' ) ):
					$x5_media_news_link = get_field( 'x5_media_news_link' ); ?>
					<a class="c-btn" href="<?php echo esc_url( $x5_media_news_link['url'] ); ?>" target="<?php echo esc_attr( $x5_media_news_link['target'] ); ?>"><?php echo esc_html( $x5_media_news_link['title'] ); ?></a>
				<?php endif; ?>

      </div>
      <!-- o-container -->

    </section>
    <!-- c-thoughts -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_media_videos_heading' ) ||
						 have_rows( 'x5_media_videos_rows' ) ): ?>

		<?php if ( get_field( 'x5_media_videos_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_media_videos_id' ) ); ?>" class="c-videos">
		<?php else: ?>
			<section class="c-videos">
		<?php endif; ?>

      <div class="o-container">

      	<?php if ( get_field( 'x5_media_videos_heading' ) ): ?>
      		<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_media_videos_heading' ) ) ); ?>
      	<?php endif; ?>

				<?php if ( have_rows( 'x5_media_videos_rows' ) ): ?>

					<div class="c-slider js-slider-videos c-slider-inner-dots">

						<?php while( have_rows( 'x5_media_videos_rows' ) ) : the_row(); ?>

							<?php if ( get_sub_field( 'x5_media_videos_rows_link' ) ||
												 get_sub_field( 'x5_media_videos_rows_pic' ) ): ?>

								<div>
			            <div class="col c-video">

			              <?php if ( get_sub_field( 'x5_media_videos_rows_link' ) ): ?>
			              	<a class="play-btn open-popup-video" href="<?php echo esc_url( get_sub_field( 'x5_media_videos_rows_link' ) ); ?>"><span></span></a>
			              <?php endif; ?>

			              <?php if ( get_sub_field( 'x5_media_videos_rows_pic' ) ):
			              	$x5_media_videos_rows_pic = get_sub_field( 'x5_media_videos_rows_pic' );
			              	$x5_media_videos_rows_pic_size = wp_get_attachment_image_url( $x5_media_videos_rows_pic['ID'], 'media_videos_cover' ); ?>

			              	<img src="<?php echo esc_url( $x5_media_videos_rows_pic_size ); ?>" alt="<?php echo esc_attr( $x5_media_videos_rows_pic['alt'] ); ?>" />

			              <?php endif; ?>

			            </div>
			          </div>

							<?php endif; ?>


						<?php endwhile; ?>

					</div>
        	<!-- c-slider -->

				<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-videos	 -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_media_social_heading' ) ||
						 get_field( 'x5_media_social_twitter_is' ) ||
						 get_field( 'x5_media_social_facebook_is' ) ): ?>

		<?php if ( get_field( 'x5_media_social_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_media_social_id' ) ); ?>" class="c-social">
		<?php else: ?>
			<section class="c-social">
		<?php endif; ?>

      <div class="o-container">

				<?php if ( get_field( 'x5_media_social_heading' ) ): ?>
					<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_media_social_heading' ) ) ); ?>
				<?php endif; ?>

				<?php if ( get_field( 'x5_media_social_twitter_is' ) ||
									 get_field( 'x5_media_social_facebook_is' ) ): ?>

					<div class="o-holder">

						<?php if ( get_field( 'x5_media_social_facebook_is' ) &&
											 get_field( 'x5_media_social_facebook_feed' ) ): ?>
							<div class="col"><?php echo get_field( 'x5_media_social_facebook_feed' ); ?></div>
						<?php endif; ?>

	          <?php if ( get_field( 'x5_media_social_twitter_is' ) &&
	          					 get_field( 'x5_media_social_twitter_feed' ) ): ?>
	          	<div class="col"><?php echo get_field( 'x5_media_social_twitter_feed' ); ?></div>
	          <?php endif; ?>

	        </div>
	        <!-- o-holder -->

				<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-social -->

	<?php endif; ?>

	<?php if ( get_field( 'x5_page_horizons_is' ) ): ?>
		<?php get_template_part( 'partials/section', 'horizons' ); ?>
	<?php endif; ?>

<?php get_footer();
