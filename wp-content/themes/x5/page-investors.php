<?php
/**
* Template Name: Investors
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

	<?php if ( get_field( 'x5_investors_intro_heading' ) ||
						 get_field( 'x5_investors_intro_desc' ) ||
						 get_field( 'x5_investors_intro_heading' ) ||
						 get_field( 'x5_investors_intro_file_heading' ) ||
						 get_field( 'x5_investors_intro_file_desc' ) ||
						 get_field( 'x5_investors_intro_file_link' ) ): ?>

		<?php if ( get_field( 'x5_investors_intro_id' ) ): ?>
      <section id="<?php echo esc_attr( get_field ( 'x5_investors_intro_id' ) ); ?>" class="c-intro">
    <?php else: ?>
      <section class="c-intro">
    <?php endif; ?>

      <div class="o-container">

        <div class="block right">

        	<?php if ( get_field( 'x5_investors_intro_heading' ) ): ?>
        		<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_investors_intro_heading' ) ) ); ?>
        	<?php endif; ?>

          <?php if ( get_field( 'x5_investors_intro_desc' ) ): ?>
          	<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_investors_intro_desc' ) ) ); ?></div>
          <?php endif; ?>

					<?php if ( get_field( 'x5_investors_intro_file_heading' ) ||
										 get_field( 'x5_investors_intro_file_desc' ) ||
										 get_field( 'x5_investors_intro_file_link' ) ): ?>

						<div class="download">
	            <span class="col">

	            	<?php if ( get_field( 'x5_investors_intro_file_heading' ) ): ?>
	            		<span class="title"><?php echo esc_html( get_field( 'x5_investors_intro_file_heading' ) ); ?></span>
	            	<?php endif; ?>

	              <?php if ( get_field( 'x5_investors_intro_file_desc' ) ): ?>
	              	<span><?php echo esc_html( get_field( 'x5_investors_intro_file_desc' ) ); ?></span>
	              <?php endif; ?>

	            </span>

	            <?php if ( get_field( 'x5_investors_intro_file_link' ) ):
	            	$x5_investors_intro_file_link = get_field( 'x5_investors_intro_file_link' ); ?>
	            	<span class="col">
	            		<a class="c-link" href="<?php echo esc_url( $x5_investors_intro_file_link['url'] ); ?>">
	            			<span class="icon-download"></span>
	            			<?php echo esc_html( $x5_investors_intro_file_link['title'] ); ?>
	            		</a>
	            	</span>
	            <?php endif; ?>

	          </div>
	          <!-- download -->

					<?php endif; ?>

        </div>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-intro -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_investors_events_heading' ) ||
						 get_field( 'x5_investors_events_is' ) ): ?>

		<?php if ( get_field( 'x5_investors_events_id' ) ): ?>
      <section id="<?php echo esc_attr( get_field ( 'x5_investors_events_id' ) ); ?>" class="c-calendar">
    <?php else: ?>
      <section class="c-calendar">
    <?php endif; ?>

      <div class="o-container">

				<?php if ( get_field( 'x5_investors_events_heading' ) ): ?>
					<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_investors_events_heading' ) ) ); ?>
				<?php endif; ?>

       	<?php if ( get_field( 'x5_investors_events_is' ) ): ?>
       		<?php get_template_part( 'partials/section', 'events' ); ?>
       	<?php endif; ?>

      </div>
      <!-- o-container -->

    </section>
    <!-- c-calendar -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_investors_thoughts_is' ) ): ?>
		<?php get_template_part( 'partials/section', 'thoughts' ); ?>
	<?php endif; ?>


	<?php if ( get_field( 'x5_investors_quote_text' ) ||
						 get_field( 'x5_investors_quote_author' ) ||
						 get_field( 'x5_investors_quote_about_pic' ) ||
						 get_field( 'x5_investors_quote_about_position' ) ||
						 get_field( 'x5_investors_quote_about_name' ) ||
						 get_field( 'x5_investors_quote_about_desc' ) ): ?>

		<?php if ( get_field( 'x5_investors_quote_id' ) ): ?>
      <section id="<?php echo esc_attr( get_field ( 'x5_investors_quote_id' ) ); ?>" class="c-quick-contact">
    <?php else: ?>
      <section class="c-quick-contact">
    <?php endif; ?>

      <div class="o-container">

				<?php if ( get_field( 'x5_investors_quote_text' ) ): ?>
					<div class="text"><p><?php echo esc_html( get_field( 'x5_investors_quote_text' ) ); ?></p></div>
				<?php endif; ?>

        <?php if ( get_field( 'x5_investors_quote_author' ) ): ?>
        	<span class="author"><?php echo esc_html( get_field( 'x5_investors_quote_author' ) ); ?></span>
        <?php endif; ?>

        <?php if ( get_field( 'x5_investors_quote_about_pic' ) ||
									 get_field( 'x5_investors_quote_about_position' ) ||
									 get_field( 'x5_investors_quote_about_name' ) ||
									 get_field( 'x5_investors_quote_about_desc' ) ): ?>

	        <div class="contact">

	        	<?php if ( get_field( 'x5_investors_quote_about_pic' ) ):
	        		$x5_investors_quote_about_pic = get_field( 'x5_investors_quote_about_pic' );
	        		$x5_investors_quote_about_pic_size = wp_get_attachment_image_url( $x5_investors_quote_about_pic['ID'], 'investors_quote_thumb' ); ?>

	        		<span class="img">
	        			<img src="<?php echo esc_url( $x5_investors_quote_about_pic_size ); ?>" alt="<?php echo esc_attr( $x5_investors_quote_about_pic['alt'] ); ?>" />
	        		</span>

	        	<?php endif; ?>

	         	<?php if ( get_field( 'x5_investors_quote_about_position' ) ): ?>
	         		<span class="position"><?php echo esc_html( get_field( 'x5_investors_quote_about_position' ) ); ?></span>
	         	<?php endif; ?>

	    			<?php if ( get_field( 'x5_investors_quote_about_name' ) ): ?>
	    				<span class="name"><?php echo esc_html( get_field( 'x5_investors_quote_about_name' ) ); ?></span>
	    			<?php endif; ?>

	          <?php if ( get_field( 'x5_investors_quote_about_desc' ) ): ?>
	          	<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_investors_quote_about_desc' ) ) ); ?></div>
	          <?php endif; ?>

	        </div>
	        <!-- contact -->

      	<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-quick-contact -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_page_horizons_is' ) ): ?>
    <?php get_template_part( 'partials/section', 'horizons' ); ?>
  <?php endif; ?>

<?php get_footer();
