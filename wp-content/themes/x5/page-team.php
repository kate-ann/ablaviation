<?php
/**
* Template Name: Team
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

	<?php if ( get_field( 'x5_page_intro_heading' ) ): ?>

		<?php if ( get_field( 'x5_page_intro_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_page_intro_id' ) ); ?>" class="c-intro--inner">
		<?php else: ?>
			<section class="c-intro--inner">
		<?php endif; ?>

      <div class="o-container">

      	<?php if ( get_field( 'x5_page_intro_heading' ) ): ?>
      		<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_page_intro_heading' ) ) ); ?>
      	<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-intro--inner -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_team_team_heading' ) ||
						 get_field( 'x5_team_team_desc' ) ||
						 have_rows( 'x5_team_team_rows' ) ): ?>

		<?php if ( get_field( 'x5_team_team_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_team_team_id' ) ); ?>" class="c-team">
		<?php else: ?>
			<section class="c-team">
		<?php endif; ?>

      <div class="o-container">

				<?php if ( get_field( 'x5_team_team_heading' ) ): ?>
					<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_team_team_heading' ) ) ); ?>
				<?php endif; ?>

        <?php if ( get_field( 'x5_team_team_desc' ) ): ?>
        	<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_team_team_desc' ) ) ); ?></div>
        <?php endif; ?>

				<?php if ( have_rows( 'x5_team_team_rows' ) ):
					$x5_team_team_row = 1; ?>

					<div class="o-holder">

						<?php while( have_rows( 'x5_team_team_rows' ) ) : the_row(); ?>

							<div class="col">

								<?php if ( get_sub_field( 'x5_team_team_rows_pic' ) ):
									$x5_team_team_rows_pic = get_sub_field( 'x5_team_team_rows_pic' );
									$x5_team_team_rows_pic_size = wp_get_attachment_image_url( $x5_team_team_rows_pic['ID'], 'team_member_thumb' ); ?>

									<span class="img open-popup-content" href="#person-0<?php echo esc_html( $x5_team_team_row ); ?>">
										<img src="<?php echo esc_url( $x5_team_team_rows_pic_size ); ?>" alt="<?php echo esc_attr( $x5_team_team_rows_pic['alt'] ); ?>" />
									</span>

								<?php endif; ?>

		            <?php if ( get_sub_field( 'x5_team_team_rows_name' ) ): ?>
		            	<span class="name"><?php echo esc_html( get_sub_field( 'x5_team_team_rows_name' ) ); ?></span>
		            <?php endif; ?>

		           	<?php if ( get_sub_field( 'x5_team_team_rows_position' ) ): ?>
		           		<span class="position"><?php echo esc_html( get_sub_field( 'x5_team_team_rows_position' ) ); ?></span>
		           	<?php endif; ?>

		            <?php if ( get_sub_field( 'x5_team_team_rows_fullname' ) ): ?>
		            	<span class="full-name"><?php echo esc_html( get_sub_field( 'x5_team_team_rows_fullname' ) ); ?></span>
		            <?php endif; ?>


		            <div id="person-0<?php echo esc_html( $x5_team_team_row ); ?>" class="popup-content popup-person mfp-hide">

		              <?php if ( get_sub_field( 'x5_team_team_rows_name' ) ): ?>
		            	<span class="name"><?php echo esc_html( get_sub_field( 'x5_team_team_rows_name' ) ); ?></span>
			            <?php endif; ?>

			           	<?php if ( get_sub_field( 'x5_team_team_rows_position' ) ): ?>
			           		<span class="position"><?php echo esc_html( get_sub_field( 'x5_team_team_rows_position' ) ); ?></span>
			           	<?php endif; ?>

			            <?php if ( get_sub_field( 'x5_team_team_rows_fullname' ) ): ?>
			            	<span class="full-name"><?php echo esc_html( get_sub_field( 'x5_team_team_rows_fullname' ) ); ?></span>
			            <?php endif; ?>

									<?php if ( get_sub_field( 'x5_team_team_rows_details' ) ): ?>
										<div class="contact-details"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_team_team_rows_details' ) ) ); ?></div>
									<?php endif; ?>

		              <?php if ( get_sub_field( 'x5_team_team_rows_bio' ) ): ?>
		              	<div class="bio"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_team_team_rows_bio' ) ) ); ?></div>
		              <?php endif; ?>

		            </div>
		            <!-- popup-content -->

		          </div>
		          <!-- col -->

		          <?php $x5_team_team_row ++; ?>
						<?php endwhile; ?>

					</div>

				<?php endif; ?>

				<?php if ( get_field( 'x5_team_team_quote_text' ) ||
									 get_field( 'x5_team_team_quote_author' ) ): ?>

					<div class="quote">

						<?php if ( get_field( 'x5_team_team_quote_text' ) ): ?>
							<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_team_team_quote_text' ) ) ); ?>
						<?php endif; ?>

	          <?php if ( get_field( 'x5_team_team_quote_author' ) ): ?>
	          	<span class="person"><?php echo esc_html( get_field( 'x5_team_team_quote_author' ) ); ?></span>
	          <?php endif; ?>

	          <span class="icon">“</span>

	        </div>
	        <!-- quote -->

				<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-team -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_page_horizons_is' ) ): ?>
		<?php get_template_part( 'partials/section', 'horizons' ); ?>
	<?php endif; ?>

<?php get_footer();
