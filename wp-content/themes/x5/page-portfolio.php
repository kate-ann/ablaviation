<?php
/**
* Template Name: Portfolio
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

	<?php if ( get_field( 'x5_page_intro_heading' ) ): ?>

		<?php if ( get_field( 'x5_page_intro_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_page_intro_id' ) ); ?>" class="c-intro--inner">
		<?php else: ?>
			<section class="c-intro--inner">
		<?php endif; ?>

      <div class="o-container">

      	<?php if ( get_field( 'x5_page_intro_heading' ) ): ?>
      		<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_page_intro_heading' ) ) ); ?>
      	<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-intro--inner -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_portfolio_investing_heading' ) ||
						 get_field( 'x5_portfolio_investing_desc' ) ||
						 get_field( 'x5_portfolio_investing_about_heading' ) ||
						 get_field( 'x5_portfolio_investing_about_desc' ) ): ?>

		<?php if ( get_field( 'x5_portfolio_investing_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_portfolio_investing_id' ) ); ?>" class="c-investing">
		<?php else: ?>
			<section class="c-investing">
		<?php endif; ?>

			<?php if ( get_field( 'x5_portfolio_investing_heading' ) ||
								 get_field( 'x5_portfolio_investing_desc' ) ): ?>

				<div class="intro">
					<div class="o-container">

						<?php if ( get_field( 'x5_portfolio_investing_heading' ) ): ?>
							<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_portfolio_investing_heading' ) ) ); ?>
						<?php endif; ?>

						<?php if ( get_field( 'x5_portfolio_investing_desc' ) ): ?>
							<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_portfolio_investing_desc' ) ) ); ?></div>
						<?php endif; ?>

					</div>
					<!-- o-container -->
				</div>
				<!-- intro -->

			<?php endif; ?>

			<?php if ( get_field( 'x5_portfolio_investing_about_heading' ) ||
								 get_field( 'x5_portfolio_investing_about_desc	' ) ): ?>

				<div class="info">
					<div class="o-container">
						<div class="block right">

							<?php if ( get_field( 'x5_portfolio_investing_about_heading' ) ): ?>
								<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_portfolio_investing_about_heading' ) ) ); ?>
							<?php endif; ?>

							<?php if ( get_field( 'x5_portfolio_investing_about_desc' ) ): ?>
								<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_portfolio_investing_about_desc' ) ) ); ?></div>
							<?php endif; ?>

						</div>
						<!-- block -->
					</div>
					<!-- o-container -->
				</div>
				<!-- info -->

			<?php endif; ?>

		</section>
		<!-- c-solutions -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_portfolio_marketing_heading' ) ||
						 have_rows( 'x5_portfolio_marketing_rows' ) ): ?>

		<?php if ( get_field( 'x5_portfolio_marketing_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_portfolio_marketing_id' ) ); ?>" class="c-points c-network">
		<?php else: ?>
			<section class="c-points c-network">
		<?php endif; ?>

	  	<div class="o-container">

				<?php if ( get_field( 'x5_portfolio_marketing_heading' ) ): ?>
					<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_portfolio_marketing_heading' ) ) ); ?>
				<?php endif; ?>

	  		<?php if ( have_rows( 'x5_portfolio_marketing_rows' ) ): ?>

	  			<div class="o-holder">

	  				<?php while( have_rows( 'x5_portfolio_marketing_rows' ) ) : the_row(); ?>

							<?php if ( get_sub_field( 'x5_portfolio_marketing_rows_heading' ) ): ?>
								<div class="col"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_portfolio_marketing_rows_heading' ) ) ); ?></div>
							<?php endif; ?>

	  				<?php endwhile; ?>

	  			</div>
					<!-- o-holder -->

	  		<?php endif; ?>

	  	</div>
	  	<!-- o-container -->
	  </section>
	  <!-- c-points -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_portfolio_analysis_heading' ) ||
						 have_rows( 'x5_portfolio_analysis_rows' ) ||
						 get_field( 'x5_portfolio_management_heading' ) ||
						 have_rows( 'x5_portfolio_management_rows' ) ): ?>

		<?php if ( get_field( 'x5_portfolio_analysis_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_portfolio_analysis_id' ) ); ?>" class="c-benefits">
		<?php else: ?>
			<section class="c-benefits">
		<?php endif; ?>

			<?php if ( get_field( 'x5_portfolio_analysis_heading' ) ||
						 		 have_rows( 'x5_portfolio_analysis_rows' ) ): ?>

				<section class="c-points c-points--short">
			  	<div class="o-container">

						<?php if ( get_field( 'x5_portfolio_analysis_heading' ) ): ?>
							<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_portfolio_analysis_heading' ) ) ); ?>
						<?php endif; ?>

			  		<?php if ( have_rows( 'x5_portfolio_analysis_rows' ) ): ?>

			  			<div class="o-holder">

			  				<?php while( have_rows( 'x5_portfolio_analysis_rows' ) ) : the_row(); ?>

			  					<?php if ( get_sub_field( 'x5_portfolio_analysis_rows_heading' ) ): ?>
			  						<div class="col"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_portfolio_analysis_rows_heading' ) ) ); ?></div>
			  					<?php endif; ?>

			  				<?php endwhile; ?>

			  			</div>
							<!-- o-holder -->

			  		<?php endif; ?>

					</div>
			  	<!-- o-container -->
			  </section>
			  <!-- c-points -->

			<?php endif; ?>


			<?php if ( get_field( 'x5_portfolio_management_heading' ) ||
								 have_rows( 'x5_portfolio_management_rows' ) ): ?>

				<div class="c-points">
					<div class="o-container">

						<?php if ( get_field( 'x5_portfolio_management_heading' ) ): ?>
							<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_portfolio_management_heading' ) ) ); ?>
						<?php endif; ?>

						<?php if ( have_rows( 'x5_portfolio_management_rows' ) ): ?>

							<div class="o-holder">

								<?php while( have_rows( 'x5_portfolio_management_rows' ) ) : the_row(); ?>

									<?php if ( get_sub_field( 'x5_portfolio_management_rows_heading' ) ): ?>
										<div class="col"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_portfolio_management_rows_heading' ) ) ); ?></div>
									<?php endif; ?>

								<?php endwhile; ?>

							</div>
							<!-- o-holder -->

						<?php endif; ?>

					</div>
					<!-- o-container -->
				</div>
				<!-- c-points -->

			<?php endif; ?>

		</div>
		<!-- c-benefits -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_page_horizons_is' ) ): ?>
		<?php get_template_part( 'partials/section', 'horizons' ); ?>
	<?php endif; ?>

<?php get_footer();
