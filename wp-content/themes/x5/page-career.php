<?php
/**
* Template Name: Career
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

	<?php if ( get_field( 'x5_career_intro_heading' ) ||
						 get_field( 'x5_career_intro_desc' ) ||
						 get_field( 'x5_career_intro_link' ) ): ?>

		<?php if ( get_field( 'x5_career_intro_id' ) ): ?>
      <section id="<?php echo esc_attr( get_field ( 'x5_career_intro_id' ) ); ?>" class="c-intro">
    <?php else: ?>
      <section class="c-intro">
    <?php endif; ?>

      <div class="o-container">
        <div class="block right">

        	<?php if ( get_field( 'x5_career_intro_heading' ) ): ?>
        		<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_career_intro_heading' ) ) ); ?>
        	<?php endif; ?>

          <?php if ( get_field( 'x5_career_intro_desc' ) ): ?>
          	<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_career_intro_desc' ) ) ); ?></div>
          <?php endif; ?>

          <?php if ( get_field( 'x5_career_intro_link' ) ):
          	$x5_career_intro_link = get_field( 'x5_career_intro_link' ); ?>
          	<a class="c-btn c-btn--alt c-btn--no-fill" href="<?php echo esc_url( $x5_career_intro_link['url'] ); ?>" target="<?php echo esc_attr( $x5_career_intro_link['target'] ); ?>"><?php echo esc_html( $x5_career_intro_link['title'] ); ?></a>
          <?php endif; ?>

        </div>
        <!-- block right -->
      </div>
    </section>
    <!-- c-intro -->

	<?php endif; ?>


	<?php if ( have_rows( 'x5_career_vacancies_rows' ) ||
						 have_rows( 'x5_career_people_rows' ) ): ?>

		<?php if ( get_field( 'x5_career_career_id' ) ): ?>
      <section id="<?php echo esc_attr( get_field ( 'x5_career_career_id' ) ); ?>" class="c-details">
    <?php else: ?>
      <section class="c-details">
    <?php endif; ?>

      <div class="o-container">

        <div class="o-holder">

					<?php if ( have_rows( 'x5_career_vacancies_rows' ) ): ?>

						<div class="col jobs">
            	<div class="c-slider js-slider-fade">

							<?php while( have_rows( 'x5_career_vacancies_rows' ) ) : the_row(); ?>

								<?php if ( get_sub_field( 'x5_career_vacancies_rows_pic' ) ||
													 get_sub_field( 'x5_career_vacancies_rows_heading' ) ||
													 get_sub_field( 'x5_career_vacancies_rows_position' ) ||
													 get_sub_field( 'x5_career_vacancies_rows_location' ) ||
													 get_sub_field( 'x5_career_vacancies_rows_link' ) ): ?>

									<div>

										<?php if ( get_sub_field( 'x5_career_vacancies_rows_pic' ) ):
											$x5_career_vacancies_rows_pic = get_sub_field( 'x5_career_vacancies_rows_pic' );
											$x5_career_vacancies_rows_pic_size = wp_get_attachment_image_url( $x5_career_vacancies_rows_pic['ID'], 'career_job_thumb' ); ?>

											<img src="<?php echo esc_url( $x5_career_vacancies_rows_pic_size ); ?>" alt="<?php echo esc_attr( $x5_career_vacancies_rows_pic['alt'] ); ?>" />

										<?php endif; ?>

										<?php if ( get_sub_field( 'x5_career_vacancies_rows_heading' ) ): ?>
											<h4><?php echo esc_html( get_sub_field( 'x5_career_vacancies_rows_heading' ) ); ?></h4>
										<?php endif; ?>

		                <?php if ( get_sub_field( 'x5_career_vacancies_rows_position' ) ): ?>
		                	<span class="position"><?php echo esc_html( get_sub_field( 'x5_career_vacancies_rows_position' ) ); ?></span>
		                <?php endif; ?>

		                <?php if ( get_sub_field( 'x5_career_vacancies_rows_location' ) ): ?>
		                	<span class="location"><?php echo esc_html( get_sub_field( 'x5_career_vacancies_rows_location' ) ); ?></span>
		                <?php endif; ?>

		                <?php if ( get_sub_field( 'x5_career_vacancies_rows_link' ) ):
		                	$x5_career_vacancies_rows_link = get_sub_field( 'x5_career_vacancies_rows_link' ); ?>
		                	<a class="c-btn c-btn--alt" href="<?php echo esc_url( $x5_career_vacancies_rows_link['url'] ); ?>" target="<?php echo esc_attr( $x5_career_vacancies_rows_link['target'] ); ?>"><?php echo esc_html( $x5_career_vacancies_rows_link['title'] ); ?></a>
		                <?php endif; ?>

		              </div>

								<?php endif; ?>


							<?php endwhile; ?>

							</div>
            	<!-- c-slider -->
          	</div>
          	<!-- col -->

					<?php endif; ?>


					<?php if ( have_rows( 'x5_career_people_rows' ) ): ?>

						<div class="col people">
            	<div class="c-slider js-slider-fade">

								<?php while( have_rows( 'x5_career_people_rows' ) ) : the_row(); ?>

									<?php if ( get_sub_field( 'x5_career_people_rows_pic' ) ||
														 get_sub_field( 'x5_career_people_rows_heading' ) ||
														 get_sub_field( 'x5_career_people_rows_desc' ) ||
														 get_sub_field( 'x5_career_people_rows_name' ) ||
														 get_sub_field( 'x5_career_people_rows_position' ) ||
														 get_sub_field( 'x5_career_people_rows_location' ) ): ?>

										<div>

											<?php if ( get_sub_field( 'x5_career_people_rows_pic' ) ):
												$x5_career_people_rows_pic = get_sub_field( 'x5_career_people_rows_pic' );
												$x5_career_people_rows_pic_size = wp_get_attachment_image_url( $x5_career_people_rows_pic['ID'], 'career_job_thumb' ); ?>

												<img src="<?php echo esc_url( $x5_career_people_rows_pic_size ); ?>" alt="<?php echo esc_attr( $x5_career_people_rows_pic['alt'] ); ?>" />

											<?php endif; ?>

			               	<?php if ( get_sub_field( 'x5_career_people_rows_heading' ) ): ?>
			               		<h5><?php echo esc_html( get_sub_field( 'x5_career_people_rows_heading' ) ); ?></h5>
			               	<?php endif; ?>

			                <?php if ( get_sub_field( 'x5_career_people_rows_desc' ) ): ?>
			                	<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_career_people_rows_desc' ) ) ); ?></div>
			                <?php endif; ?>

			               	<?php if ( get_sub_field( 'x5_career_people_rows_name' ) ): ?>
			               		<span class="name"><?php echo esc_html( get_sub_field( 'x5_career_people_rows_name' ) ); ?></span>
			               	<?php endif; ?>

			               	<?php if ( get_sub_field( 'x5_career_people_rows_position' ) ): ?>
			               		<span class="position"><?php echo esc_html( get_sub_field( 'x5_career_people_rows_position' ) ); ?></span>
			               	<?php endif; ?>

			                <?php if ( get_sub_field( 'x5_career_people_rows_location' ) ): ?>
			                	<span class="location"><?php echo esc_html( get_sub_field( 'x5_career_people_rows_location' ) ); ?></span>
			                <?php endif; ?>

			              </div>

									<?php endif; ?>

								<?php endwhile; ?>

							</div>
	            <!-- c-slider -->
	          </div>
	          <!-- col -->

					<?php endif; ?>

        </div>
        <!-- o-holder -->
      </div>
      <!-- o-container -->
    </section>
    <!-- c-details -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_career_benefits_heading' ) ||
						 get_field( 'x5_career_benefits_rows' ) ): ?>

		<?php if ( get_field( 'x5_career_benefits_id' ) ): ?>
      <section id="<?php echo esc_attr( get_field ( 'x5_career_benefits_id' ) ); ?>" class="c-points c-points--quadro c-working">
    <?php else: ?>
      <section class="c-points c-points--quadro c-working">
    <?php endif; ?>

      <div class="o-container">

				<?php if ( get_field( 'x5_career_benefits_heading' ) ): ?>
					<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_career_benefits_heading' ) ) ); ?>
				<?php endif; ?>

				<?php if ( have_rows( 'x5_career_benefits_rows' ) ): ?>

					<div class="o-holder">

						<?php while( have_rows( 'x5_career_benefits_rows' ) ) : the_row(); ?>

							<?php if ( get_sub_field( 'x5_career_benefits_rows_heading' ) ): ?>
								<div class="col"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_career_benefits_rows_heading' ) ) ); ?></div>
							<?php endif; ?>

						<?php endwhile; ?>

					</div>
        	<!-- o-holder -->

				<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-points -->

	<?php endif; ?>


	<section class="c-instagram">
		<?php echo do_shortcode( '[instagram-feed]' ); ?>
	</section>
	<!-- c-instagram -->

	<?php if ( get_field( 'x5_career_social_twitter_is' ) ||
						 get_field( 'x5_career_social_facebook_is' ) ): ?>

		<?php if ( get_field( 'x5_career_social_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_career_social_id' ) ); ?>" class="c-social">
		<?php else: ?>
			<section class="c-social">
		<?php endif; ?>

      <div class="o-container">

				<?php if ( get_field( 'x5_career_social_twitter_is' ) ||
									 get_field( 'x5_career_social_facebook_is' ) ): ?>

					<div class="o-holder">

						<?php if ( get_field( 'x5_career_social_facebook_is' ) &&
											 get_field( 'x5_career_social_facebook_feed' ) ): ?>
							<div class="col">
								<span class="icon-fb"></span>
								<?php echo get_field( 'x5_career_social_facebook_feed' ); ?>
							</div>
						<?php endif; ?>

	          <?php if ( get_field( 'x5_career_social_twitter_is' ) &&
	          					 get_field( 'x5_career_social_twitter_feed' ) ): ?>
	          	<div class="col">
	          		<span class="icon-tw"></span>
	          		<?php echo get_field( 'x5_career_social_twitter_feed' ); ?>
	          	</div>
	          <?php endif; ?>

	        </div>
	        <!-- o-holder -->

				<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-social -->

	<?php endif; ?>

	<?php if ( get_field( 'x5_page_horizons_is' ) ): ?>
		<?php get_template_part( 'partials/section', 'horizons' ); ?>
	<?php endif; ?>

<?php get_footer();
