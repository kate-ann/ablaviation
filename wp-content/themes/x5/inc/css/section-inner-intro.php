<?php

	// Inner Intro section
	if ( get_field( 'x5_page_intro_bg_m' ) ):
		$x5_page_intro_bg_m = get_field( 'x5_page_intro_bg_m' );
		?>
			.c-intro--inner {
			  background: transparent url('<?php echo esc_url( $x5_page_intro_bg_m['url'] ); ?>') <?php echo esc_html( get_field( 'x5_page_intro_bg_m_x' ) ); ?> <?php echo esc_html( get_field( 'x5_page_intro_bg_m_y' ) ); ?> no-repeat;
			  background-size: cover;
			}
		<?php
	endif;

	if ( get_field( 'x5_page_intro_bg_d' ) ):
		$x5_page_intro_bg_d = get_field( 'x5_page_intro_bg_d' );
		?>
			@media screen and (min-width: 768px) {
				.c-intro--inner {
				  background: transparent url('<?php echo esc_url( $x5_page_intro_bg_d['url'] ); ?>') <?php echo esc_html( get_field( 'x5_page_intro_bg_d_x' ) ); ?> <?php echo esc_html( get_field( 'x5_page_intro_bg_d_y' ) ); ?> no-repeat;
				  background-size: cover;
				}
			}
		<?php
	endif;

	if ( get_field( 'x5_page_intro_heading_color' ) ):
		?>
			.c-intro--inner h1,
			.c-intro--inner h2,
			.c-intro--inner h3,
			.c-intro--inner h4,
			.c-intro--inner h5,
			.c-intro--inner h6 {
			  color: <?php echo esc_html( get_field( 'x5_page_intro_heading_color' ) ); ?>;
			}
		<?php
	endif;




