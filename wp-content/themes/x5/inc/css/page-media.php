<?php

	// Media page
	if ( is_page_template( 'page-media.php' ) ):

		// News section
		if ( get_field( 'x5_media_news_bg' ) ):
			$x5_media_news_bg = get_field( 'x5_media_news_bg' );
			?>
				.page-media .c-news {
				  background: transparent url('<?php echo esc_url( $x5_media_news_bg['url'] ); ?>') center 0 no-repeat;
				  background-size: cover;
				}

			<?php
		endif;

		// Videos section
		if ( get_field( 'x5_media_videos_bg' ) ):
			$x5_media_videos_bg = get_field( 'x5_media_videos_bg' );
			?>
				.page-media .c-videos {
				  background: #f3f3fa url('<?php echo esc_url( $x5_media_videos_bg['url'] ); ?>') 0 0 no-repeat;
				  background-size: cover;
				}

			<?php
		endif;

	endif;
	// end of Media page