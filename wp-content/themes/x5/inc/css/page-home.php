<?php

	// Home page
	if ( is_page_template( 'page-home.php' ) ):

		// Intro section
		if ( get_field( 'x5_home_intro_bg' ) ):

			$x5_home_intro_bg = get_field( 'x5_home_intro_bg' );
			?>

			.page-home .c-intro {
			  background: transparent url('<?php echo esc_url( $x5_home_intro_bg['url'] ); ?>') center center no-repeat;
			  background-size: cover;
			}

			<?php
		endif;


		// People section
		if ( get_field( 'x5_home_people_bg' ) ):

			$x5_home_people_bg = get_field( 'x5_home_people_bg' );
			?>

			.page-home .c-people {
			  background: transparent url('<?php echo esc_url( $x5_home_people_bg['url'] ); ?>') 50% 0 no-repeat;
			  background-size: cover;
			}

			<?php
		endif;

		if ( get_field( 'x5_home_people_video_bg' ) ):

			$x5_home_people_video_bg = get_field( 'x5_home_people_video_bg' );
			?>

			.page-home .c-people .video {
			  background: transparent url('<?php echo esc_url( $x5_home_people_video_bg['url'] ); ?>') center 0 no-repeat;
			  background-size: cover;
			}

			<?php
		endif;
		// People section


		// Solutions section
		if ( have_rows( 'x5_home_solutions_rows' ) ):
			$x5_home_solutions_row = 1;

			while( have_rows( 'x5_home_solutions_rows' ) ) : the_row();

				// image + width
				if ( get_sub_field( 'x5_home_solutions_rows_pic' ) &&
						 get_sub_field( 'x5_home_solutions_rows_iconw' ) ):
					$x5_home_solutions_rows_pic = get_sub_field( 'x5_home_solutions_rows_pic' );
					?>

					.page-home .c-solutions .solutions li:nth-child(<?php echo esc_html( $x5_home_solutions_row ); ?>) {
					  background: transparent url('<?php echo esc_url( $x5_home_solutions_rows_pic['url'] ); ?>') center 0 no-repeat;
					  background-size: <?php echo esc_html( get_sub_field( 'x5_home_solutions_rows_iconw' ) ); ?> auto;
					}

					<?php

				// just image
				elseif ( get_sub_field( 'x5_home_solutions_rows_pic' ) ) :
					$x5_home_solutions_rows_pic = get_sub_field( 'x5_home_solutions_rows_pic' );

					?>

					.page-home .c-solutions .solutions li:nth-child(<?php echo esc_html( $x5_home_solutions_row ); ?>) {
					  background: transparent url('<?php echo esc_url( $x5_home_solutions_rows_pic['url'] ); ?>') center 0 no-repeat;
					}

					<?php
				endif;

				// image + top + left
				if ( get_sub_field( 'x5_home_solutions_rows_pic' ) &&
						 get_sub_field( 'x5_home_solutions_rows_iconl' ) &&
						 get_sub_field( 'x5_home_solutions_rows_icont' ) ):
					?>

						@media screen and (min-width: 955px) {
						  .page-home .c-solutions .solutions li:nth-child(<?php echo esc_html( $x5_home_solutions_row ); ?>) {
						    background-position: <?php echo esc_html( get_sub_field( 'x5_home_solutions_rows_iconl' ) ); ?> <?php echo esc_html( get_sub_field( 'x5_home_solutions_rows_icont' ) ); ?>;
						  }
						}

					<?php
				endif;

			?>

			<?php
				$x5_home_solutions_row ++;

			endwhile;
		endif;
		// Solutions

		// News
		if ( get_field( 'x5_home_events_bg' ) ):

			$x5_home_events_bg = get_field( 'x5_home_events_bg' );
			?>

			.page-home .c-events {
			  background: transparent url('<?php echo esc_url( $x5_home_events_bg['url'] ); ?>') center 0 no-repeat;
			  background-size: cover;
			}

			<?php
		endif;
		// News section

	endif;
	// end of Home page