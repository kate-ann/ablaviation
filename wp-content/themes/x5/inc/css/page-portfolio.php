<?php

	// Portfolio page
	if ( is_page_template( 'page-portfolio.php' ) ):

		if ( get_field( 'x5_portfolio_investing_bg') ):
			$x5_portfolio_investing_bg = get_field( 'x5_portfolio_investing_bg' );
			?>
				.page-portfolio .c-investing .info {
				  background: #f3f3fa url('<?php echo esc_url( $x5_portfolio_investing_bg['url'] ); ?>') 0 100% no-repeat;
				}

				@media screen and (min-width: 1132px) {
				.page-portfolio .c-investing .info {
						background-position: 0 center;
					  background-size: auto 100%;
					  }
					}
			<?php
		endif;

	endif;
	// end of Portfolio page