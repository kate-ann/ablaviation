<?php

	// Horizons section
	if ( get_field( 'x5_general_horizons_bg', 'option' ) ):
		$x5_general_horizons_bg = get_field( 'x5_general_horizons_bg', 'option' );
		?>
			.c-horizons {
			  background: transparent url('<?php echo esc_url( $x5_general_horizons_bg['url'] ); ?>') center 0 no-repeat;
			  background-size: cover;
			}
		<?php
	endif;