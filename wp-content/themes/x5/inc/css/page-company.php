<?php
	// Company page
	if ( is_page_template( 'page-company.php' ) ):

		// Thoughts section
		if ( get_field( 'x5_company_thoughts_bg' ) ):
			$x5_company_thoughts_bg = get_field( 'x5_company_thoughts_bg' );
			?>
				.page-company .c-thoughts {
				  background: transparent url('<?php echo esc_url( $x5_company_thoughts_bg['url'] ); ?>') center 0 no-repeat;
				  background-size: cover;
				}
			<?php
		endif;

		// News section
		if ( get_field( 'x5_company_social_bg' ) ):
			$x5_company_social_bg = get_field( 'x5_company_social_bg' );
			?>
				.page-company .c-news {
				  background: #f3f3fa url('<?php echo esc_url( $x5_company_social_bg['url'] ); ?>') center 0 no-repeat;
				  background-size: cover;
				}
			<?php
		endif;

		// Contact form section
		if ( get_field( 'x5_company_contact_bg' ) ):
			$x5_company_contact_bg = get_field( 'x5_company_contact_bg' );
			?>
				.page-company .c-contact-form {
				  background: #f3f3fa url('<?php echo esc_url( $x5_company_contact_bg['url'] ); ?>') center 0 no-repeat;
				  background-size: cover;
				}
			<?php
		endif;

	endif;
	// end of Company page