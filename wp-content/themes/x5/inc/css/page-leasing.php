<?php

	// Leasing page
	if ( is_page_template( 'page-leasing-service.php' ) ):

		// Services section
		if ( get_field( 'x5_leasing_services_bg' ) ):
			$x5_leasing_services_bg = get_field( 'x5_leasing_services_bg' );
			?>
				.page-leasing .c-services {
				  background: #1f1e53 url('<?php echo esc_url( $x5_leasing_services_bg['url'] ); ?>') 0 0 no-repeat;
				  background-size: cover;
				}

			<?php
		endif;

		// Solutions section
		if ( get_field( 'x5_leasing_solutions_bg' ) ):
			$x5_leasing_solutions_bg = get_field( 'x5_leasing_solutions_bg' );
			?>
				.page-leasing .c-solutions {
				  background: #1f1e53 url('<?php echo esc_url( $x5_leasing_solutions_bg['url'] ); ?>') 100% 0 no-repeat;
				  background-size: cover;
				}

			<?php
		endif;

	endif;
	// end of Leasing page