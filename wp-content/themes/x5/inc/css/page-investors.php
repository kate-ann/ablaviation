<?php

	if ( is_page_template( 'page-investors.php' ) ):

		// Inner Intro section
		if ( get_field( 'x5_investors_intro_bg_m' ) ):
			$x5_investors_intro_bg_m = get_field( 'x5_investors_intro_bg_m' );
			?>
				.page-investors .c-intro {
				  background: transparent url('<?php echo esc_url( $x5_investors_intro_bg_m['url'] ); ?>') <?php echo esc_html( get_field( 'x5_investors_intro_bg_m_x' ) ); ?> <?php echo esc_html( get_field( 'x5_investors_intro_bg_m_y' ) ); ?> no-repeat;
				  background-size: cover;
				}
			<?php
		endif;

		if ( get_field( 'x5_investors_intro_bg_d' ) ):
			$x5_investors_intro_bg_d = get_field( 'x5_investors_intro_bg_d' );
			?>
				@media screen and (min-width: 768px) {
					.page-investors .c-intro {
					  background: transparent url('<?php echo esc_url( $x5_investors_intro_bg_d['url'] ); ?>') <?php echo esc_html( get_field( 'x5_investors_intro_bg_d_x' ) ); ?> <?php echo esc_html( get_field( 'x5_investors_intro_bg_d_y' ) ); ?> no-repeat;
					  background-size: cover;
					}
				}
			<?php
		endif;

		// Innovative Thoughts section
		if( 'x5_investors_thoughts_bg' ):
			$x5_investors_thoughts_bg = get_field( 'x5_investors_thoughts_bg' );
			?>
			.page-investors .c-reports {
				background: #fff url('<?php echo esc_url( $x5_investors_thoughts_bg['url'] ); ?>') 0 100% no-repeat;
	  		background-size: cover;
			}
			@media screen and (min-width: 1110px) {
				.page-investors .c-reports {
					background-size: contain;
				}
			}
			<?php
		endif;

	endif;
	// end of Investors page