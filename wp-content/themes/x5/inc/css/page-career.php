<?php

	// Career page
	if ( is_page_template( 'page-career.php' ) ):

		// Intro section
		if ( get_field( 'x5_career_intro_bg_m' ) ):
			$x5_career_intro_bg_m = get_field( 'x5_career_intro_bg_m' );
			?>
				.page-career .c-intro {
				  background: transparent url('<?php echo esc_url( $x5_career_intro_bg_m['url'] ); ?>') <?php echo esc_html( get_field( 'x5_career_intro_bg_m_x' ) ); ?> <?php echo esc_html( get_field( 'x5_career_intro_bg_m_y' ) ); ?> no-repeat;
				  background-size: cover;
				}
			<?php
		endif;

		if ( get_field( 'x5_career_intro_bg_d' ) ):
			$x5_career_intro_bg_d = get_field( 'x5_career_intro_bg_d' );
			?>
				@media screen and (min-width: 768px) {
					.page-career .c-intro {
					  background: transparent url('<?php echo esc_url( $x5_career_intro_bg_d['url'] ); ?>') <?php echo esc_html( get_field( 'x5_career_intro_bg_d_x' ) ); ?> <?php echo esc_html( get_field( 'x5_career_intro_bg_d_y' ) ); ?> no-repeat;
					  background-size: cover;
					}
				}
			<?php
		endif;

	endif;
	// end of Career page