<?php

	// Section Map
	if ( have_rows( 'x5_general_map_rows', 'option' ) ):
		$x5_general_map_row = 1;

		while( have_rows( 'x5_general_map_rows', 'option' ) ) : the_row();

			if ( get_sub_field( 'x5_general_map_rows_top', 'option' ) &&
					 get_sub_field( 'x5_general_map_rows_left', 'option' ) ):
				?>

				.c-map .markers li:nth-child(<?php echo esc_html( $x5_general_map_row ); ?>) {
				  top: <?php echo esc_html( get_sub_field( 'x5_general_map_rows_top', 'option' ) ); ?>;
				  left: <?php echo esc_html( get_sub_field( 'x5_general_map_rows_left', 'option' ) ); ?>;
				}

				<?php
			endif;

			$x5_general_map_row ++;
		endwhile;

	endif;