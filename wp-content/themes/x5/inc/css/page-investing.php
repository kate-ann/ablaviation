<?php

	// Investing services page
	if ( is_page_template( 'page-investing-service.php' ) ):

		// Services section
		if ( get_field( 'x5_investing_services_bg' ) ):
			$x5_investing_services_bg = get_field( 'x5_investing_services_bg' );
			?>
				.page-investing .c-services {
				  background: transparent url('<?php echo esc_url( $x5_investing_services_bg['url'] ); ?>') 0 bottom no-repeat;
				}
			<?php
		endif;


		if ( have_rows( 'x5_investing_services_rows' ) ):
			$x5_investing_services_row = 1;

			while( have_rows( 'x5_investing_services_rows' ) ) : the_row();

				// SVG image + width
				if ( get_sub_field( 'x5_investing_services_rows_icon' ) &&
						 get_sub_field( 'x5_investing_services_rows_iconw' ) &&
						 get_sub_field( 'x5_investing_services_rows_iconl' ) &&
						 get_sub_field( 'x5_investing_services_rows_icont' ) ):
					$x5_investing_services_rows_icon = get_sub_field( 'x5_investing_services_rows_icon' );

					?>

					.page-investing .c-services .col:nth-child(<?php echo esc_html( $x5_investing_services_row ); ?>) {
					  background: transparent url('<?php echo esc_url( $x5_investing_services_rows_icon['url'] ); ?>') <?php echo esc_html( get_sub_field( 'x5_investing_services_rows_iconl' ) ); ?> <?php echo esc_html( get_sub_field( 'x5_investing_services_rows_icont' ) ); ?> no-repeat;
					  background-size: <?php echo esc_html( get_sub_field( 'x5_investing_services_rows_iconw' ) ); ?> auto;
					}

					<?php

				// PNG image + position
				elseif ( get_sub_field( 'x5_investing_services_rows_icon' )  &&
								 get_sub_field( 'x5_investing_services_rows_iconl' ) &&
								 get_sub_field( 'x5_investing_services_rows_icont' )  ) :
					$x5_investing_services_rows_icon = get_sub_field( 'x5_investing_services_rows_icon' );

					?>

						.page-investing .c-services .col:nth-child(<?php echo esc_html( $x5_investing_services_row ); ?>) {
					  background: transparent url('<?php echo esc_url( $x5_investing_services_rows_icon['url'] ); ?>') <?php echo esc_html( get_sub_field( 'x5_investing_services_rows_iconl' ) ); ?> <?php echo esc_html( get_sub_field( 'x5_investing_services_rows_icont' ) ); ?> no-repeat;
					}

					<?php

				// PNG image
				elseif ( get_sub_field( 'x5_investing_services_rows_icon' ) ) :
					$x5_investing_services_rows_icon = get_sub_field( 'x5_investing_services_rows_icon' );

					?>

						.page-investing .c-services .col:nth-child(<?php echo esc_html( $x5_investing_services_row ); ?>) {
					  background: transparent url('<?php echo esc_url( $x5_investing_services_rows_icon['url'] ); ?>') center 60px no-repeat;
						}

					<?php
				endif;

				$x5_investing_services_row ++;
			endwhile;
		endif;

	endif;
	// end of Investing page