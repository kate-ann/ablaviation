<?php
/**
* Template Name: Home
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

	<?php if ( get_field( 'x5_home_intro_subheading' ) ||
						 get_field( 'x5_home_intro_heading' ) ||
						 get_field( 'x5_home_intro_desc' ) ||
						 get_field( 'x5_home_intro_link' ) ): ?>

		<?php if ( get_field( 'x5_home_intro_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_home_intro_id' ) ); ?>" class="c-intro">
		<?php else: ?>
			<section class="c-intro">
		<?php endif; ?>

			<div class="block left">

				<?php if ( get_field( 'x5_home_intro_subheading' ) ): ?>
					<p class="subheading"><?php echo esc_html( get_field( 'x5_home_intro_subheading' ) ); ?></p>
				<?php endif; ?>

	    	<?php if ( get_field( 'x5_home_intro_heading' ) ): ?>
	    		<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_intro_heading' ) ) ); ?>
	    	<?php endif; ?>

				<?php if ( get_field( 'x5_home_intro_desc' ) ): ?>
					<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_intro_desc' ) ) ); ?></div>
				<?php endif; ?>

				<?php if ( get_field( 'x5_home_intro_link' ) ):
					$x5_home_intro_link = get_field( 'x5_home_intro_link' ); ?>
					<a class="c-btn c-btn--animate" href="<?php echo esc_url( $x5_home_intro_link['url'] ); ?>" target="<?php echo esc_attr( $x5_home_intro_link['target'] ); ?>"><?php echo esc_html( $x5_home_intro_link['title'] ); ?></a>
				<?php endif; ?>

	    </div>

			<?php if ( get_field( 'x5_home_intro_video_mp4' ) ||
			 					 get_field( 'x5_home_intro_video_webm' ) ||
			 					 get_field( 'x5_home_intro_video_yt' ) ): ?>

			 	<div id="js-intro-player" class="c-player" style="width: 100%; height: 875px; position: absolute;"></div>

			<?php endif; ?>

		</section>
		<!-- c-intro -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_home_news_heading' ) ||
						 get_field( 'x5_home_news_desc' ) ||
						 get_field( 'x5_home_news_link' ) ||
						 get_field( 'x5_home_news_is' ) ): ?>

		<?php if ( get_field( 'x5_home_news_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_home_news_id' ) ); ?>" class="c-news">
		<?php else: ?>
			<section class="c-news">
		<?php endif; ?>

      <div class="o-container">

				<?php if ( get_field( 'x5_home_news_heading' ) ||
									 get_field( 'x5_home_news_desc' ) ||
									 get_field( 'x5_home_news_link' ) ): ?>

					<div class="block left">

	          <?php if ( get_field( 'x5_home_news_heading' ) ): ?>
	          	<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_news_heading' ) ) ); ?>
	          <?php endif; ?>

	          <?php if ( get_field( 'x5_home_news_desc' ) ): ?>
	          	<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_news_desc' ) ) ); ?></div>
	          <?php endif; ?>

	          <?php if ( get_field( 'x5_home_news_link' ) ):
	          	$x5_home_news_link = get_field( 'x5_home_news_link' ); ?>
	          	<a class="c-btn c-btn--no-fill c-btn--animate" href="<?php echo esc_url( $x5_home_news_link['url'] ); ?>" target="<?php echo esc_attr( $x5_home_news_link['target'] ); ?>"><?php echo esc_html( $x5_home_news_link['title'] ); ?></a>
	          <?php endif; ?>

	        </div>
	        <!-- block left -->

				<?php endif; ?>

				<?php if ( get_field( 'x5_home_news_is' ) ): ?>

					<div class="news">

	          <?php if ( get_field( 'x5_home_news_subheading' ) ): ?>
	          	<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_news_subheading' ) ) ); ?>
	          <?php endif; ?>

	          <?php if ( have_rows( 'x5_general_news_rows', 'option' ) ):
	          	$x5_general_news_row = 1;
	          	$x5_general_news_rows_total = count( get_field( 'x5_general_news_rows', 'option' ) );
							?>

	          	<div class="c-slider js-slider-events">

	          		<?php while( have_rows( 'x5_general_news_rows', 'option' ) ) : the_row(); ?>

									<?php if ( $x5_general_news_row > $x5_general_news_rows_total-3 ): ?>

										<div>

											<div class="event">

			          				<?php if ( get_sub_field( 'x5_general_news_rows_date', 'option' ) ): ?>
			          					<span class="date-posted"><?php echo esc_html( get_sub_field( 'x5_general_news_rows_date', 'option' ) ); ?></span>
			          				<?php endif; ?>

					              <?php if ( get_sub_field( 'x5_general_news_rows_heading' ) ): ?>
					              	<span class="title"><?php echo esc_html( get_sub_field( 'x5_general_news_rows_heading', 'option' ) ); ?></span>
					              <?php endif; ?>

					              <?php if ( get_sub_field( 'x5_general_news_rows_edate', 'option' ) ): ?>
					              	<span class="date">
					              		<span class="icon-calendar-opened"></span>
					              		<?php echo esc_html( get_sub_field( 'x5_general_news_rows_edate', 'option' ) ); ?>
					              	</span>
					              <?php endif; ?>

					              <?php if ( get_sub_field( 'x5_general_news_rows_elocation', 'option' ) ): ?>
					              	<span class="location">
					              		<span class="icon-marker"></span>
					              		<?php echo esc_html( get_sub_field( 'x5_general_news_rows_elocation', 'option' ) ); ?>
					              	</span>
					              <?php endif; ?>

					              <?php if ( get_sub_field( 'x5_general_news_rows_excerpt', 'option' ) ): ?>
					              	<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_general_news_rows_excerpt', 'option' ) ) ); ?></div>
					              <?php endif; ?>

												<?php if ( get_sub_field( 'x5_general_news_rows_link', 'option' ) ):
													$x5_general_news_rows_link = get_sub_field( 'x5_general_news_rows_link', 'option' ); ?>
													<a class="c-link" href="<?php echo esc_url( $x5_general_news_rows_link['url'] ); ?>" target="<?php echo esc_attr( $x5_general_news_rows_link['target'] ); ?>"><?php echo esc_html( $x5_general_news_rows_link['title'] ); ?></a>
												<?php endif; ?>

					            </div>
					            <!-- event -->

					          </div>

									<?php endif; ?>

									<?php $x5_general_news_row ++; ?>

	          		<?php endwhile; ?>

	          	</div>
	          	<!-- c-slider -->

	          <?php endif; ?>

	        </div>
	        <!-- news -->

				<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-news -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_home_people_heading' ) ||
						 get_field( 'x5_home_people_desc' ) ||
						 get_field( 'x5_home_people_link' ) ||
						 get_field( 'x5_home_people_video_link' ) ): ?>

		<?php if ( get_field( 'x5_home_people_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_home_people_id' ) ); ?>" class="c-people">
		<?php else: ?>
			<section class="c-people">
		<?php endif; ?>

      <div class="o-container">

				<?php if ( get_field( 'x5_home_people_video_link' ) ):
					$x5_home_people_video_link = get_field( 'x5_home_people_video_link' ); ?>
					<div class="video">
	          <a href="<?php echo esc_url( $x5_home_people_video_link['url'] ); ?>" class="play-btn open-popup-video">
	            <span></span>
	          </a>
	        </div>
				<?php endif; ?>

				<?php if ( get_field( 'x5_home_people_heading' ) ||
									 get_field( 'x5_home_people_desc' ) ||
									 get_field( 'x5_home_people_link' ) ): ?>

					<div class="block right">

	          <?php if ( get_field( 'x5_home_people_heading' ) ): ?>
	          	<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_people_heading' ) ) ); ?>
	          <?php endif; ?>

	          <?php if ( get_field( 'x5_home_people_desc' ) ): ?>
	          	<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_people_desc' ) ) ); ?></div>
	          <?php endif; ?>

	          <?php if ( get_field( 'x5_home_people_link' ) ):
	          	$x5_home_people_link = get_field( 'x5_home_people_link' ); ?>
	          	<a class="c-btn c-btn--light c-btn--animate" href="<?php echo esc_url( $x5_home_people_link['url'] ); ?>" target="<?php echo esc_attr( $x5_home_people_link['target'] ); ?>"><?php echo esc_html( $x5_home_people_link['title'] ); ?></a>
	          <?php endif; ?>

	        </div>
	        <!-- block -->

				<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-people -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_home_solutions_heading' ) ||
						 get_field( 'x5_home_solution_desc' ) ||
						 get_field( 'x5_home_solutions_link' ) ||
						 have_rows( 'x5_home_solutions_rows' ) ): ?>

		<?php if ( get_field( 'x5_home_solutions_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_home_solutions_id' ) ); ?>" class="c-solutions">
		<?php else: ?>
			<section class="c-solutions">
		<?php endif; ?>

      <div class="o-container">

      	<?php if ( get_field( 'x5_home_solutions_heading' ) ||
									 get_field( 'x5_home_solutions_desc' ) ||
									 get_field( 'x5_home_solutions_link' ) ): ?>

					<div class="block left">

	          <?php if ( get_field( 'x5_home_solutions_heading' ) ): ?>
	          	<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_solutions_heading' ) ) ); ?>
	          <?php endif; ?>

	          <?php if ( get_field( 'x5_home_solutions_desc' ) ): ?>
	          	<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_solutions_desc' ) ) ); ?></div>
	          <?php endif; ?>

	          <?php if ( get_field( 'x5_home_solutions_link' ) ):
	          	$x5_home_solutions_link = get_field( 'x5_home_solutions_link' ); ?>
	          	<a class="c-btn c-btn--no-fill c-btn--animate" href="<?php echo esc_url( $x5_home_solutions_link['url'] ); ?>" target="<?php echo esc_attr( $x5_home_solutions_link['target'] ); ?>"><?php echo esc_html( $x5_home_solutions_link['title'] ); ?></a>
	          <?php endif; ?>

	        </div>

				<?php endif; ?>

				<?php if ( have_rows( 'x5_home_solutions_rows' ) ): ?>

					<ul class="solutions">

						<?php while( have_rows( 'x5_home_solutions_rows' ) ) : the_row(); ?>

							<?php if ( get_sub_field( 'x5_home_solutions_rows_heading' ) ): ?>
								<li><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_home_solutions_rows_heading' ) ) ); ?></li>
							<?php endif; ?>

						<?php endwhile; ?>

					</ul>
        	<!-- solutions -->

				<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-solutions -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_home_thoughts_is' ) ): ?>

		<?php get_template_part( 'partials/section', 'thoughts' ); ?>

	<?php endif; ?>


	<?php if ( get_field( 'x5_home_events_events_is' ) ||
						 get_field( 'x5_home_events_twitter_is' ) ): ?>

		<?php if ( get_field( 'x5_home_events_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_home_events_id' ) ); ?>" class="c-events">
		<?php else: ?>
			<section class="c-events">
		<?php endif; ?>

      <div class="o-container">

				<?php if ( get_field( 'x5_home_events_events_is' ) ): ?>
					<?php get_template_part( 'partials/section', 'events' ); ?>
				<?php endif; ?>

				<?php if ( get_field( 'x5_home_events_twitter_is' ) &&
									 get_field( 'x5_home_events_twitter_feed' ) ): ?>
					<div class="tweets">
						<?php echo get_field( 'x5_home_events_twitter_feed' ); ?>
					</div>
				<?php endif; ?>

      </div>
      <!-- o-container -->

    </section>
    <!-- c-events -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_home_team_heading' ) ||
						 get_field( 'x5_home_team_desc' ) ||
						 get_field( 'x5_home_team_link' ) ||
						 have_rows( 'x5_home_team_rows' ) ): ?>

		<?php if ( get_field( 'x5_home_team_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_home_team_id' ) ); ?>" class="c-careers">
		<?php else: ?>
			<section class="c-careers">
		<?php endif; ?>

      <div class="o-container">

				<?php if ( get_field( 'x5_home_team_heading' ) ||
									 get_field( 'x5_home_team_desc' ) ||
									 get_field( 'x5_home_team_link' ) ): ?>

        <div class="block left">

        	<?php if ( get_field( 'x5_home_team_heading' ) ): ?>
        		<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_team_heading' ) ) ); ?>
        	<?php endif; ?>

          <?php if ( get_field( 'x5_home_team_desc' ) ): ?>
          	<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_team_desc' ) ) ); ?></div>
          <?php endif; ?>

          <?php if ( get_field( 'x5_home_team_link' ) ):
          	$x5_home_team_link = get_field( 'x5_home_team_link' ); ?>
          	<a class="c-btn c-btn--no-fill c-btn--alt c-btn--animate" href="<?php echo esc_url( $x5_home_team_link['url'] ); ?>" target="<?php echo esc_attr( $x5_home_team_link['target'] ); ?>"><?php echo esc_html( $x5_home_team_link['title'] ); ?></a>
          <?php endif; ?>

        </div>

      	<?php endif; ?>

				<?php if ( have_rows( 'x5_home_team_rows' ) ): ?>

					<div class="c-slider js-slider-testimonials">

						<?php while( have_rows( 'x5_home_team_rows' ) ) : the_row(); ?>

							<?php if ( get_sub_field( 'x5_home_team_rows_pic' ) ||
												 get_sub_field( 'x5_home_team_rows_text' ) ||
												 get_sub_field( 'x5_home_team_rows_name' ) ||
												 get_sub_field( 'x5_home_team_rows_position' ) ||
												 get_sub_field( 'x5_home_team_rows_location' ) ): ?>

								<div>
			            <div class="info">

			            	<?php if ( get_sub_field( 'x5_home_team_rows_pic' ) ):
			            		$x5_home_team_rows_pic = get_sub_field( 'x5_home_team_rows_pic' );
			            		$x5_home_team_rows_pic_size = wp_get_attachment_image_url( $x5_home_team_rows_pic['ID'], 'home_team_thumb' ); ?>

			            		<img class="thumb" src="<?php echo esc_url( $x5_home_team_rows_pic_size ); ?>" alt="<?php echo esc_attr( $x5_home_team_rows_pic['alt'] ); ?>" />

			            	<?php endif; ?>

			              <?php if ( get_sub_field( 'x5_home_team_rows_text' ) ): ?>
			              	<div class="text"><p><?php echo esc_html( get_sub_field( 'x5_home_team_rows_text' ) ); ?></p></div>
			              <?php endif; ?>

			              <?php if ( get_sub_field( 'x5_home_team_rows_name' ) ): ?>
			              	<span class="name"><?php echo esc_html( get_sub_field( 'x5_home_team_rows_name' ) ); ?></span>
			              <?php endif; ?>

			              <?php if ( get_sub_field( 'x5_home_team_rows_position' ) ): ?>
			              	<span class="position"><?php echo esc_html( get_sub_field( 'x5_home_team_rows_position' ) ); ?></span>
			              <?php endif; ?>

			              <?php if ( get_sub_field( 'x5_home_team_rows_location' ) ): ?>
			              	<span class="location"><?php echo esc_html( get_sub_field( 'x5_home_team_rows_location' ) ); ?></span>
			              <?php endif; ?>
			            </div>
			          </div>

							<?php endif; ?>

						<?php endwhile; ?>

					</div>
        	<!-- c-slider -->

				<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-careers -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_home_contact_heading' ) ||
						 get_field( 'x5_home_contact_desc' ) ||
						 get_field( 'x5_home_contact_map_is' ) ||
						 get_field( 'x5_home_contact_social_is' ) ): ?>


		<section id="section-contact" class="c-contact">
      <div class="o-container">

        <div class="block right">

          <?php if ( get_field( 'x5_home_contact_heading' ) ): ?>
          	<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_contact_heading' ) ) ); ?>
          <?php endif; ?>

          <?php if ( get_field( 'x5_home_contact_desc' ) ): ?>
          	<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_contact_desc' ) ) ); ?></div>
          <?php endif; ?>

          <?php if ( get_field( 'x5_home_contact_social_is' ) ): ?>
          	<?php get_template_part( 'partials/social', 'buttons' ); ?>
          <?php endif; ?>

        </div>
        <!-- block right -->

				<?php if ( get_field( 'x5_home_contact_map_is' ) ): ?>
					<?php get_template_part( 'partials/section', 'map' ); ?>
				<?php endif; ?>

      </div>
      <!-- o-container -->

      <a href="#" class="btn-up"><?php echo esc_html( 'UP' ); ?></a>

    </section>
    <!-- c-contact -->

	<?php endif; ?>


	<?php if ( get_field( 'x5_page_horizons_is' ) ): ?>
		<?php get_template_part( 'partials/section', 'horizons' ); ?>
	<?php endif; ?>

<?php get_footer();
