<?php
/**
* Template Name: Investing Service
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

	<?php if ( get_field( 'x5_page_intro_heading' ) ): ?>

		<?php if ( get_field( 'x5_page_intro_id' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_page_intro_id' ) ); ?>" class="c-intro--inner">
		<?php else: ?>
			<section class="c-intro--inner">
		<?php endif; ?>

      <div class="o-container">

      	<?php if ( get_field( 'x5_page_intro_heading' ) ): ?>
      		<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_page_intro_heading' ) ) ); ?>
      	<?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-intro--inner -->

	<?php endif; ?>


  <?php if ( get_field( 'x5_investing_services_heading' ) ||
             get_field( 'x5_investing_services_desc' ) ||
             get_field( 'x5_investing_services_rows' ) ||
             get_field( 'x5_investing_services_about_desc' ) ): ?>

    <?php if ( get_field( 'x5_investing_services_id' ) ): ?>
      <section id="<?php echo esc_attr( get_field ( 'x5_investing_services_id' ) ); ?>" class="c-services">
    <?php else: ?>
      <section class="c-services">
    <?php endif; ?>

      <div class="services">

        <div class="o-container">

          <?php if ( get_field( 'x5_investing_services_heading' ) ): ?>
           <?php echo wp_kses_post( force_balance_tags( get_field( 'x5_investing_services_heading' ) ) ); ?>
          <?php endif; ?>

          <?php if ( get_field( 'x5_investing_services_desc' ) ): ?>
            <div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_investing_services_desc' ) ) ); ?></div>
          <?php endif; ?>

          <?php if ( have_rows( 'x5_investing_services_rows' ) ): ?>

            <div class="o-holder">

              <?php while( have_rows( 'x5_investing_services_rows' ) ) : the_row(); ?>

                <?php if ( get_sub_field( 'x5_investing_services_rows_heading' ) ||
                           get_sub_field( 'x5_investing_services_rows_desc' ) ): ?>

                  <div class="col">

                    <?php if ( get_sub_field( 'x5_investing_services_rows_heading' ) ): ?>
                      <?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_investing_services_rows_heading' ) ) ); ?>
                    <?php endif; ?>

                    <?php if ( get_sub_field( 'x5_investing_services_rows_desc' ) ): ?>
                      <p><?php echo esc_html( get_sub_field( 'x5_investing_services_rows_desc' ) ); ?></p>
                    <?php endif; ?>

                  </div>
                  <!-- col -->

                <?php endif; ?>

              <?php endwhile; ?>

            </div>
            <!-- o-holder -->

          <?php endif; ?>

        </div>
        <!-- o-container -->

      </div>
      <!-- services -->

      <div class="about">
        <div class="o-container">
          <div class="info">
            <p>
              <strong>ABL Aviation is a fully independent, innovative and dedicated third-party asset and investment manager.</strong>
            </p>
            <p>ABL Aviation offers aircraft investors outstanding service. Our team is very capable and experienced and we make sure to maintain a close relationship with each one of our investors. All assets are managed in-house, guaranteeing that our investors
              needs will be met in an efficient and timely manner, and that they will remain our priority, with no risk of conflicts of interest.</p>
          </div>
        </div>
      </div>
      <!-- about -->

    </section>
    <!-- c-services -->

  <?php endif; ?>


  <?php if ( get_field( 'x5_investing_expertise_heading' ) ||
             have_rows( 'x5_investing_expertise_rows' ) ): ?>

    <?php if ( get_field( 'x5_investing_expertise_id' ) ): ?>
      <section id="<?php echo esc_attr( get_field ( 'x5_investing_expertise_id' ) ); ?>" class="c-points c-points--quadro c-expertise">
    <?php else: ?>
      <section class="c-points c-points--quadro c-expertise">
    <?php endif; ?>

      <div class="o-container">

        <?php if ( get_field( 'x5_investing_expertise_heading' ) ): ?>
          <?php echo wp_kses_post( force_balance_tags( get_field( 'x5_investing_expertise_heading' ) ) ); ?>
        <?php endif; ?>

        <?php if ( have_rows( 'x5_investing_expertise_rows' ) ): ?>

          <div class="o-holder">

            <?php while( have_rows( 'x5_investing_expertise_rows' ) ) : the_row(); ?>

              <?php if ( get_sub_field( 'x5_investing_expertise_rows_heading' ) ): ?>
                <div class="col"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_investing_expertise_rows_heading' ) ) ); ?></div>
              <?php endif; ?>

            <?php endwhile; ?>

          </div>
          <!-- o-holder -->

        <?php endif; ?>

      </div>
      <!-- o-container -->
    </section>
    <!-- c-points -->

  <?php endif; ?>


  <?php if ( get_field( 'x5_investing_contact_pic' ) ||
             get_field( 'x5_investing_contact_desc' ) ||
             get_field( 'x5_investing_contact_events_btn_is' ) ): ?>

    <section class="c-quick-contact">
      <div class="o-container">

        <?php if ( get_field( 'x5_investing_contact_pic' ) ):
          $x5_investing_contact_pic = get_field( 'x5_investing_contact_pic' );
          $x5_investing_contact_pic_size = wp_get_attachment_image_url( $x5_investing_contact_pic['ID'], 'investing_contact_thumb' ); ?>

          <img src="<?php echo esc_url( $x5_investing_contact_pic_size ); ?>" alt="<?php echo esc_attr( $x5_investing_contact_pic['alt'] ); ?>" />

        <?php endif; ?>

        <?php if ( get_field( 'x5_investing_contact_desc' ) ): ?>
          <?php echo wp_kses_post( force_balance_tags( get_field( 'x5_investing_contact_desc' ) ) ); ?>
        <?php endif; ?>

        <?php if ( get_field( 'x5_investing_contact_events_btn_is' ) ): ?>
          <a href="#lightbox-with-events" class="c-btn c-btn--no-fill all-events open-popup-content">
            <span class="icon-calendar-opened"></span><?php echo esc_html( 'NEXT EVENTS' ); ?>
          </a>
        <?php endif; ?>

      </div>
      <!-- o-container -->

      <?php if ( get_field( 'x5_investing_contact_events_btn_is' ) ): ?>
        <?php get_template_part( 'partials/section-all', 'events' ); ?>
      <?php endif; ?>

    </section>
    <!-- c-quick-contact -->

  <?php endif; ?>


  <?php if ( get_field( 'x5_page_horizons_is' ) ): ?>
    <?php get_template_part( 'partials/section', 'horizons' ); ?>
  <?php endif; ?>

<?php get_footer();
