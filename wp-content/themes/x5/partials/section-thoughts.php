
<?php if ( get_field( 'x5_general_thoughts_heading', 'option' ) ||
					 get_field( 'x5_general_thoughts_desc', 'option' ) ||
					 get_field( 'x5_general_thoughts_link', 'option' ) ||
					 get_field( 'x5_general_thoughts_subheading', 'option' ) ||
					 get_field( 'x5_general_thoughts_pic', 'option' ) ): ?>

	<?php if ( get_field( 'x5_general_thoughts_id', 'option' ) ): ?>
			<section id="<?php echo esc_attr( get_field ( 'x5_general_thoughts_id', 'option' ) ); ?>" class="c-reports">
		<?php else: ?>
			<section class="c-reports">
		<?php endif; ?>

      <div class="o-container">
        <div class="block right">

					<?php if ( get_field( 'x5_general_thoughts_heading', 'option' ) ): ?>
						<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_general_thoughts_heading', 'option' ) ) ); ?>
					<?php endif; ?>

          <?php if ( get_field( 'x5_general_thoughts_desc', 'option' ) ): ?>
          	<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_general_thoughts_desc', 'option' ) ) ); ?></div>
          <?php endif; ?>

         	<?php if ( get_field( 'x5_general_thoughts_link', 'option' ) ||
										 get_field( 'x5_general_thoughts_subheading', 'option' ) ||
										 get_field( 'x5_general_thoughts_pic', 'option' ) ): ?>

						<div class="partner">

							<?php if ( get_field( 'x5_general_thoughts_link', 'option' ) ):
								$x5_general_thoughts_link = get_field( 'x5_general_thoughts_link', 'option' ); ?>
								<a class="c-btn c-btn--no-fill" href="<?php echo esc_url( $x5_general_thoughts_link['url'] ); ?>" target="<?php echo esc_attr( $x5_general_thoughts_link['target'] ); ?>"><span class="icon-graph"></span><?php echo esc_html( $x5_general_thoughts_link['title'] ); ?></a>
							<?php endif; ?>

	            <?php if ( get_field( 'x5_general_thoughts_subheading', 'option' ) ): ?>
	            	<span class="part"><?php echo esc_html( get_field( 'x5_general_thoughts_subheading', 'option' ) ); ?></span>
	            <?php endif; ?>

	            <?php if ( get_field( 'x5_general_thoughts_pic', 'option' ) ):
	            	$x5_general_thoughts_pic = get_field( 'x5_general_thoughts_pic', 'option' ); ?>

	            	<img class="logo" src="<?php echo esc_url( $x5_general_thoughts_pic['url'] ); ?>" alt="<?php echo esc_attr( $x5_general_thoughts_pic['alt'] ); ?>">

	            <?php endif; ?>

	          </div>
	          <!-- partner -->

					<?php endif; ?>

        </div>
        <!-- block right -->

      </div>
      <!-- o-container -->
    </section>
    <!-- c-reports -->

<?php endif; ?>