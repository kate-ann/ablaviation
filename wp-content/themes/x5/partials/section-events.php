<?php if ( have_rows( 'x5_general_events_rows', 'option' ) ):
  $x5_general_events_row = 1; ?>

  <div class="events">

    <?php while( have_rows( 'x5_general_events_rows', 'option' ) ) : the_row(); ?>

      <?php if ( $x5_general_events_row <= 3 ) : ?>

        <?php if ( get_sub_field( 'x5_general_events_rows_date', 'option' ) ||
                   get_sub_field( 'x5_general_events_rows_title', 'option' ) ): ?>

          <div class="c-event">
            <span class="icon-calendar-opened"></span>

            <?php if ( get_sub_field( 'x5_general_events_rows_date', 'option' ) ): ?>
              <span class="date"><?php echo esc_html( get_sub_field( 'x5_general_events_rows_date', 'option' ) ); ?></span>
            <?php endif; ?>

            <?php if ( get_sub_field( 'x5_general_events_rows_title', 'option' ) ): ?>
              <span class="title"><?php echo esc_html( get_sub_field( 'x5_general_events_rows_title', 'option' ) ); ?></span>
            <?php endif; ?>

            <?php if ( get_sub_field( 'x5_general_events_rows_location', 'option' ) ): ?>
              <span class="location"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_general_events_rows_location', 'option' ) ) ); ?></span>
            <?php endif; ?>


            <div title="Add to Calendar" class="c-btn c-btn--no-fill addeventatc">
              <span class="icon-calendar"></span>ADD TO CALENDAR

              <?php if ( get_sub_field( 'x5_general_events_rows_start', 'option' ) ): ?>
                <span class="start"><?php echo esc_html( get_sub_field( 'x5_general_events_rows_start', 'option' ) ); ?></span>
              <?php endif; ?>

              <?php if ( get_sub_field( 'x5_general_events_rows_end', 'option' ) ): ?>
                <span class="end"><?php echo esc_html( get_sub_field( 'x5_general_events_rows_end', 'option' ) ); ?></span>
              <?php endif; ?>

              <?php if ( get_sub_field( 'x5_general_events_rows_timezone', 'option' ) ): ?>
                <span class="timezone"><?php echo esc_html( get_sub_field( 'x5_general_events_rows_timezone', 'option' ) ); ?></span>
              <?php endif; ?>

              <?php if ( get_sub_field( 'x5_general_events_rows_title', 'option' ) ): ?>
                <span class="title"><?php echo esc_html( get_sub_field( 'x5_general_events_rows_title', 'option' ) ); ?></span>
              <?php endif; ?>

              <span class="description"></span>

              <?php if ( get_sub_field( 'x5_general_events_rows_location', 'option' ) ): ?>
                <span class="location"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_general_events_rows_location', 'option' ) ) ); ?></span>
              <?php endif; ?>

            </div>
          </div>
          <!-- c-event -->

        <?php endif; ?>

      <?php endif; ?>

      <?php $x5_general_events_row++; ?>

    <?php endwhile; ?>

    <a href="#lightbox-with-events" class="c-btn c-btn--light all-events open-popup-content">
    <span class="icon-calendar"></span><?php echo esc_html( 'EVENTS CALENDAR' ); ?></a>

  </div>
  <!-- events -->

<?php endif; ?>

<?php get_template_part( 'partials/section-all', 'events' ); ?>









