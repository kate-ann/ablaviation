<?php if ( get_field( 'x5_general_map_pic', 'option' ) ||
           have_rows( 'x5_general_map_rows', 'option' ) ): ?>

  <div class="c-map">

    <?php if ( get_field( 'x5_general_map_pic', 'option' ) ):
      $x5_general_map_pic = get_field( 'x5_general_map_pic', 'option' ); ?>
      <img src="<?php echo esc_url( $x5_general_map_pic['url'] ); ?>" alt="<?php echo esc_attr( $x5_general_map_pic['alt'] ); ?>">
    <?php endif; ?>

    <?php if ( have_rows( 'x5_general_map_rows', 'option' ) ):
      $x5_general_map_row = 1; ?>

      <ul class="markers">

        <?php while( have_rows( 'x5_general_map_rows', 'option' ) ) : the_row(); ?>

          <li class="location<?php echo esc_attr( $x5_general_map_row ); ?> wow bounceInUp" data-wow-delay="0ms" data-wow-duration="500ms">
            <span></span>
            <div id="location<?php echo esc_attr( $x5_general_map_row ); ?>" style="display: none;">
              <div class="info">

                <?php if ( get_sub_field( 'x5_general_map_rows_pic', 'option' ) ):
                  $general_map_location_thumb = get_sub_field( 'x5_general_map_rows_pic', 'option' );
                  $general_map_location_thumb_size = wp_get_attachment_image_url( $general_map_location_thumb['ID'], 'general_map_location_thumb' ); ?>

                  <img src="<?php echo esc_url( $general_map_location_thumb_size ); ?>" alt="<?php echo esc_attr( $general_map_location_thumb['alt'] ); ?>" />

                <?php endif; ?>

                <?php if ( get_sub_field( 'x5_general_map_rows_desc', 'option' ) ): ?>
                  <div class="desc"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_general_map_rows_desc', 'option' ) ) ); ?></div>
                <?php endif; ?>

              </div>
              <!-- info -->
            </div>
          </li>

          <?php $x5_general_map_row ++; ?>

        <?php endwhile; ?>

      </ul>

    <?php endif; ?>

  </div>

<?php endif; ?>

