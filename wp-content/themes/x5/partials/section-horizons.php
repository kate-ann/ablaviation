<?php if ( get_field( 'x5_general_horizons_subheading', 'option' ) ||
           get_field( 'x5_general_horizons_heading', 'option' ) ||
           have_rows( 'x5_general_horizons_rows', 'option' ) ): ?>

  <section id="section-horizons" class="c-horizons">
    <div class="o-container">
      <div class="block left">

        <?php if ( get_field( 'x5_general_horizons_subheading', 'option' ) ): ?>
          <p class="subheading"><?php echo esc_html( get_field( 'x5_general_horizons_subheading', 'option' ) ); ?></p>
        <?php endif; ?>

        <?php if ( get_field( 'x5_general_horizons_heading', 'option' ) ): ?>
          <?php echo wp_kses_post( force_balance_tags( get_field( 'x5_general_horizons_heading', 'option' ) ) ); ?>
        <?php endif; ?>

        <?php if ( have_rows( 'x5_general_horizons_rows', 'option' ) ): ?>

          <?php while( have_rows( 'x5_general_horizons_rows', 'option' ) ) : the_row(); ?>

            <?php if ( get_sub_field( 'x5_general_horizons_rows_link', 'option' ) ):
              $x5_general_horizons_rows_link = get_sub_field( 'x5_general_horizons_rows_link', 'option' ); ?>


              <?php if ( get_sub_field( 'x5_general_horizons_rows_type', 'option' ) == 'Contact' ): ?>

                <a href="<?php echo esc_url( $x5_general_horizons_rows_link['url'] ); ?>" class="c-btn c-btn--light contact" target="<?php echo esc_attr( $x5_general_horizons_rows_link['target'] ); ?>">
                  <span class="icon-pen"></span><?php echo esc_html( $x5_general_horizons_rows_link['title'] ); ?>
                </a>

              <?php else: ?>

                <a href="<?php echo esc_url( $x5_general_horizons_rows_link['url'] ); ?>" class="c-btn c-btn--light c-btn--animate" target="<?php echo esc_attr( $x5_general_horizons_rows_link['target'] ); ?>">
                  <?php echo esc_html( $x5_general_horizons_rows_link['title'] ); ?>
                </a>

              <?php endif; ?>

            <?php endif; ?>


          <?php endwhile; ?>

        <?php endif; ?>


      </div>
    </div>
    <!-- o-container -->
  </section>
  <!-- c-horizons -->

<?php endif; ?>

