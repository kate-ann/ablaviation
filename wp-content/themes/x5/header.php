<?php
/**
 * X5: Header
 *
 * Contains dummy HTML to show the default structure of WordPress header file
 *
 * Remember to always include the wp_head() call before the ending </head> tag
 *
 * Make sure that you include the <!DOCTYPE html> in the same line as ?> closing tag
 * otherwise ajax might not work properly
 *
 * @package WordPress
 * @subpackage X5
 */
?><!DOCTYPE html>
<!--[if IE 8]> <html class="no-js ie8 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta name="viewport" content="initial-scale=1, minimum-scale=1, width=device-width">

  <title><?php echo esc_html( 'ABL Aviation - Aircraft Leasing' ); ?></title>

  <!-- optional -->
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <!-- end of optional -->

  <script>
    document.createElement('picture');
  </script>

  <script>
    document.querySelector('html').classList.remove('no-js');
  </script>

  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,600,700|Poppins:300,500,600|Source+Sans+Pro:300,300i,400,600,600i,700" rel="stylesheet">


  <!-- Dev code -->
  <script src="https://cdn.jsdelivr.net/npm/hls.js@latest"></script>
  <script type="text/javascript" src="https://addevent.com/libs/atc/1.6.1/atc.min.js" async defer></script>

  <?php if ( get_field( 'x5_header_scripts', 'option' ) ): ?>
    <?php echo get_field( 'x5_header_scripts', 'option' ); ?>
  <?php endif; ?>

  <?php

    if ( is_page_template( 'page-contact.php' ) ){
      gravity_form_enqueue_scripts( 1, true );
    }

    // do not remove
    wp_head();
  ?>

</head>

<body
  <?php
    if ( is_page_template( 'page-home.php' ) ){
      body_class( 'page-home' );
    } elseif ( is_page_template( 'page-team.php' ) ){
      body_class( 'page-team' );
    } elseif ( is_page_template( 'page-leasing-service.php' ) ){
      body_class( 'page-leasing' );
    } elseif ( is_page_template( 'page-portfolio.php' ) ){
      body_class( 'page-portfolio' );
    } elseif ( is_page_template( 'page-career.php' ) ){
      body_class( 'page-career' );
    } elseif ( is_page_template( 'page-company.php' ) ){
      body_class( 'page-company' );
    } elseif ( is_page_template( 'page-investors.php' ) ){
      body_class( 'page-investors' );
    } elseif ( is_page_template( 'page-media.php' ) ){
      body_class( 'page-media' );
    } elseif ( is_page_template( 'page-investing-service.php' ) ){
      body_class( 'page-investing' );
    } elseif ( is_page_template( 'page-custom.php' ) ){
      body_class( 'page-custom' );
    } elseif ( is_page_template( 'page-blog.php' ) ) {
      body_class( 'page-blog' );
    } elseif ( is_404()) {
      body_class( 'error404' );
    } elseif ( is_page()) {
      body_class( 'page-default' );
    }
  ?>>

  <header class="c-header">

    <div class="o-container">

      <h1 class="c-logo"><a href="<?php echo esc_url( home_url() ); ?>" rel="index" title="<?php esc_html_e( 'Go to homepage', 'x5' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>

      <div class="c-nav js-nav">

        <div class="c-nav__btn js-nav__btn"></div>
        <nav class="c-nav__menu js-nav__menu">

          <!-- / Display menu -->
          <?php
            if ( has_nav_menu( 'header-menu' ) ):
              wp_nav_menu(
                array(
                  'theme_location' => 'header-menu',
                  'container' => '',
                )
              );
            endif;
          ?>

          <?php if ( get_field( 'x5_header_link', 'option' ) ||
                     get_field( 'x5_header_social_btns_is', 'option' ) ): ?>

            <div class="utility-nav">

              <?php if ( get_field( 'x5_header_link', 'option' ) ):
                $x5_header_link = get_field( 'x5_header_link', 'option' ); ?>
                <a class="c-btn" href="<?php echo esc_url( $x5_header_link['url'] ); ?>" target="<?php echo esc_attr( $x5_header_link['target'] ); ?>"><?php echo esc_html( $x5_header_link['title'] ); ?></a>
              <?php endif; ?>

              <?php if ( get_field( 'x5_header_social_btns_is', 'option' ) ): ?>
                <?php get_template_part( 'partials/social', 'buttons' ); ?>
              <?php endif; ?>

            </div>

          <?php endif; ?>

        </nav>

      </div>
      <!-- / c-nav -->

    </div>
    <!-- / o-container -->

  </header>
  <!-- / c-header -->