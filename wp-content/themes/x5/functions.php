<?php
/**
 * X5: Theme specific functionalities
 *
 * Do not close any of the php files included with ?> closing tag!
 *
 * @package WordPress
 * @subpackage X5
 */

/*
 * Load X5 features
 */
function x5_load_features() {

	$features = scandir( dirname( __FILE__ ) . '/features/' );

	foreach ( $features as $feature ) {

		if ( current_theme_supports( $feature ) ) {
			require_once dirname( __FILE__ ) . '/features/' . $feature . '/' . $feature . '.php';
		}
	}
}

add_action( 'init', 'x5_load_features' );

/*
 * Add basic functionality required by WordPress.org
 */
if ( function_exists( add_theme_support( 'seo-title' ) ) ) {
	add_theme_support( 'seo-title' );
}

if ( function_exists( add_theme_support( 'threaded-comments' ) ) ) {
	add_theme_support( 'threaded-comments' );
}

if ( function_exists( add_theme_support( 'comments' ) ) ) {
	add_theme_support( 'comments' );
}

if ( function_exists( add_theme_support( 'automatic-feed-links' ) ) ) {
	add_theme_support( 'automatic-feed-links' );
}

if ( function_exists( add_theme_support( 'title-tag' ) ) ) {
	add_theme_support( 'title-tag' );
}

if ( function_exists( add_theme_support( 'menus' ) ) ) {
	add_theme_support( 'menus',
		array(
			'navigation-top' => __( 'Top Navigation Menu', 'x5' ),
			'navigation-footer' => __( 'Footer Navigation Menu', 'x5' ),
		)
	);
}


/*
 * Register menus
 */
function x5_register_menus() {

	register_nav_menus(
		array(
			'header-menu' => esc_html__( 'Header Menu', 'x5' ),
		)
	);
}
add_action( 'init', 'x5_register_menus' );


/*
* Add default sidebars
*/
function x5_register_sidebars() {

	register_sidebar(
			array(
				'id'            => 'primary',
				'name'          => __( 'Primary Sidebar', 'x5' ),
				'description'   => __( 'Main content sidebar.', 'x5' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
	);
	/* Repeat register_sidebar() code for additional sidebars. */
}
add_action( 'widgets_init', 'x5_register_sidebars' );


if ( ! isset( $content_width ) ) {
	$content_width = 1210;
}


/*
 * Add post thumbnails
 */
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 525, 340, true ); // true, so that the image is cropped to exact this size


/*
 * Add new image sizes
 */
if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'home_team_thumb', 128, 128, true );
	add_image_size( 'general_map_location_thumb', 300, 192, true );
	add_image_size( 'team_member_thumb', 245, 212, true );
	add_image_size( 'media_report_thumb', 180, 215, true );
	add_image_size( 'media_videos_cover', 540, 396, true );
	add_image_size( 'career_job_thumb', 131, 131, true );
	add_image_size( 'investors_quote_thumb', 252, 222, true );
	add_image_size( 'company_video_cover', 540, 396, true );
	add_image_size( 'company_document_cover', 160, 227, true );
	add_image_size( 'media_news_thumb', 255, 155, true );
	add_image_size( 'company_news_thumb', 257, 169, true );
	add_image_size( 'investing_contact_thumb', 130, 130, true );
}

/*
 * Add image sizes to uploader
 */
function x5_add_image_sizes() {
	add_image_size( 'post_image', 712, 340, true );
}
add_action( 'init', 'x5_add_image_sizes' );

function x5_show_image_sizes( $sizes ) {
	$sizes['post_image'] = __( 'Post Image', 'x5' );
 	return $sizes;
}
add_filter( 'image_size_names_choose', 'x5_show_image_sizes' );


/*
 * Load optimization
 */
// Remove Emoji icons
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


/*
 * Enqueue styles and scripts
 */
function x5_add_scripts() {

	// Header
	wp_enqueue_style( 'x5-style', get_stylesheet_uri() );

	wp_enqueue_style( 'x5-main', get_template_directory_uri() . '/css/main.css' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Footer
	wp_enqueue_script( 'x5-plugins', get_template_directory_uri() . '/js/vendor.js', array( 'jquery' ), '25082018', true );

	// Register script that we pass values to
	wp_register_script( 'x5-js', get_template_directory_uri() . '/js/app.bundle.js', array( 'jquery', 'x5-plugins' ), '25082018', true );


	// Variables passed to app.bundle.js file
	if ( get_field( 'x5_home_intro_video_mp4' ) ||
			 get_field( 'x5_home_intro_video_webm' ) ||
			 get_field( 'x5_home_intro_video_yt' ) ) {

		$x5_home_intro_video_mp4 = get_field( 'x5_home_intro_video_mp4' );
		$x5_home_intro_video_webm = get_field( 'x5_home_intro_video_webm' );
		$x5_home_intro_video_yt = get_field( 'x5_home_intro_video_yt' );

		$x5_home_intro_videos = array(
			'mp4' => esc_js( $x5_home_intro_video_mp4['url'] ),
			'webm' => esc_js( $x5_home_intro_video_webm['url'] ),
			'yt' => esc_js( $x5_home_intro_video_yt ),
		);

		// Pass js values to app.bundle.js file
		wp_localize_script( 'x5-js', 'x5_js_vars', $x5_home_intro_videos );
	}

	// Enqueue script that we pass values to
	wp_enqueue_script( 'x5-js', get_stylesheet_directory_uri() . '/js/app.bundle.js', array( 'jquery' ), '25082018', true );

}
add_action( 'wp_enqueue_scripts', 'x5_add_scripts' );


/**
 * Add defer attribute to loaded scripts
 */
function x5_add_scripts_defer_attribute( $tag, $handle ) {
	$scripts_to_defer = array( 'jquery', // remove 'jquery' is it's causing problems with other scripts
															'x5-plugins',
															'x5-js' );

	foreach( $scripts_to_defer as $defer_script ) {
		if ( $defer_script === $handle ) {
			return str_replace( ' src', ' defer="defer" src', $tag );
		}
	}
	return $tag;
}
add_filter( 'script_loader_tag', 'x5_add_scripts_defer_attribute', 10, 2 );


/*
 * Add ACF Options pages
 */
if( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page();

	acf_add_options_sub_page( 'General' );
	acf_add_options_sub_page( 'Header' );
	acf_add_options_sub_page( 'News' );
	acf_add_options_sub_page( 'Events' );
	acf_add_options_sub_page( 'Map' );
	acf_add_options_sub_page( 'Innovative Thoughts' );
	acf_add_options_sub_page( 'New Horizons' );
	acf_add_options_sub_page( 'Content Popups' );
	acf_add_options_sub_page( 'Footer' );
}


/*
 * Add styles to editor
 */
function x5_add_editor_styles() {
	add_editor_style( get_template_directory_uri() . '/css/main.css' );
}
add_action( 'admin_init', 'x5_add_editor_styles' );


/*
 * Add client defined styles to header
 */
function x5_add_customized_css() {

	?>

	<style>

		<?php

			// Header logo
			if ( get_field( 'x5_header_logo', 'option' ) &&
				 	 get_field( 'x5_header_logow', 'option' ) &&
				 	 get_field( 'x5_header_logoh', 'option' ) ):
			// SVG image
			$x5_header_logo = get_field( 'x5_header_logo', 'option' ); ?>

			.c-logo {
				width: <?php echo wp_filter_nohtml_kses( get_field( 'x5_header_logow', 'option' ) ); ?>;
				height: <?php echo wp_filter_nohtml_kses( get_field( 'x5_header_logoh', 'option' ) ); ?>;
			  background: transparent url("<?php echo esc_url( $x5_header_logo['url'] ); ?>") 0 0 no-repeat;
			  background-size: <?php echo wp_filter_nohtml_kses( get_field( 'x5_header_logow', 'option' ) ); ?> auto;
			}

			<?php
		elseif ( get_field( 'x5_header_logo', 'option' ) ):
			// PNG/Jpeg image
			$x5_header_logo = get_field( 'x5_header_logo', 'option' ); ?>

			.c-logo {
				width: <?php echo wp_filter_nohtml_kses( $x5_header_logo['width'] ); ?>px;
				height: <?php echo wp_filter_nohtml_kses( $x5_header_logo['height'] ); ?>px;
			  background: transparent url("<?php echo esc_url( $x5_header_logo['url'] ); ?>") 0 0 no-repeat;
			}

			<?php

		endif;

		// Footer logo
			if ( get_field( 'x5_footer_logo', 'option' ) &&
				 	 get_field( 'x5_footer_logow', 'option' ) &&
				 	 get_field( 'x5_footer_logoh', 'option' ) ):
			// SVG image
			$x5_footer_logo = get_field( 'x5_footer_logo', 'option' ); ?>

			.c-footer .c-logo {
				width: <?php echo wp_filter_nohtml_kses( get_field( 'x5_footer_logow', 'option' ) ); ?>;
				height: <?php echo wp_filter_nohtml_kses( get_field( 'x5_footer_logoh', 'option' ) ); ?>;
			  background: transparent url("<?php echo esc_url( $x5_footer_logo['url'] ); ?>") 0 0 no-repeat;
			  background-size: <?php echo wp_filter_nohtml_kses( get_field( 'x5_footer_logow', 'option' ) ); ?> auto;
			}

			<?php
		elseif ( get_field( 'x5_footer_logo', 'option' ) ):
			// PNG/Jpeg image
			$x5_footer_logo = get_field( 'x5_footer_logo', 'option' ); ?>

			.c-footer .c-logo {
				width: <?php echo wp_filter_nohtml_kses( $x5_footer_logo['width'] ); ?>px;
				height: <?php echo wp_filter_nohtml_kses( $x5_footer_logo['height'] ); ?>px;
			  background: transparent url("<?php echo esc_url( $x5_footer_logo['url'] ); ?>") 0 0 no-repeat;
			}

			<?php

		endif;

		get_template_part( 'inc/css/section', 'inner-intro' );
		get_template_part( 'inc/css/section', 'map' );
		get_template_part( 'inc/css/section', 'horizons' );

		get_template_part( 'inc/css/page', 'home' );
		get_template_part( 'inc/css/page', 'company' );
		get_template_part( 'inc/css/page', 'team' );
		get_template_part( 'inc/css/page', 'leasing' );
		get_template_part( 'inc/css/page', 'investing' );
		get_template_part( 'inc/css/page', 'portfolio' );
		get_template_part( 'inc/css/page', 'media' );
		get_template_part( 'inc/css/page', 'investors' );
		get_template_part( 'inc/css/page', 'career' );

			?>
	</style>
		<?php
}
add_action( 'wp_head', 'x5_add_customized_css' );


/*
 * Truncating the excerpt to 140 characters
 */
function x5_get_excerpt(){
	$excerpt = get_the_content();
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 140);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = $excerpt . esc_html( '...' );
	return $excerpt;
}

/*
 * Add shortcodes
 */
// [link_w_popup][/link_w_popup]
function x5_add_shortcode_link_w_popup( $attr, $content = null ) {
	$a = shortcode_atts( array(
		'id' => '',
	), $attr );
	return '<a class="open-popup-content" href="#' . $a['id'] . '">'. $content .'</a>';
}

/*
 * Register shortcodes
 */
function x5_register_shortcodes(){
	add_shortcode( 'link_w_popup', 'x5_add_shortcode_link_w_popup' );
}
add_action( 'init', 'x5_register_shortcodes');


/*
 *
 * Customize menu classes
 *
 * Hightlight blog menu item on single post page
 */
function x5_hightlight_blog_menu_item_on_single_post( $classes = array(), $menu_item = false ) {
    if ( is_singular( 'post' ) && 'Consciousness now' == $menu_item->title &&
            !in_array( 'current_page_item', $classes ) ) {
        $classes[] = 'current_page_item';
    }
    return $classes;
}
add_filter( 'nav_menu_css_class', 'x5_hightlight_blog_menu_item_on_single_post', 100, 2 );


/*
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';



/* Auto scroll to GravityForms confirmation message */
//add_filter( 'gform_confirmation_anchor', '__return_true' );