<?php
/**
 * X5: Footer
 *
 * Remember to always include the wp_footer() call before the </body> tag
 *
 * @package WordPress
 * @subpackage X5
 */
?>

<?php if ( get_field( 'x5_footer_logo', 'option' ) ||
					 get_field( 'x5_footer_menu', 'option' ) ||
					 get_field( 'x5_footer_form_is', 'option' ) ||
					 get_field( 'x5_footer_copyright', 'option' ) ): ?>

	<footer class="c-footer">

	  <div class="o-container">

			<?php if ( get_field( 'x5_footer_logo', 'option' ) ): ?>
				<span class="c-logo"><a href="<?php echo esc_url( home_url() ); ?>" rel="index" title="<?php esc_html_e( 'Go to homepage', 'x5' ); ?>"><?php bloginfo( 'name' ); ?></a></span>
			<?php endif; ?>

			<?php if ( get_field( 'x5_footer_menu', 'option' ) ): ?>
				<div class="secondary-nav"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_footer_menu', 'option' ) ) ); ?></div>
			<?php endif; ?>

			<div class="form">
				<p class="heading">SIGN UP FOR UPDATES</p>

				<?php if ( get_field( 'x5_footer_form_is', 'option' ) &&
									 get_field( 'x5_footer_form_id', 'option' ) ): ?>
					<?php gravity_form( esc_html( get_field( 'x5_footer_form_id', 'option' ) ), false, false, false, '', true, false ); ?>
				<?php endif; ?>
			</div>
			<!-- form -->

			<?php if ( get_field( 'x5_footer_copyright', 'option' ) ): ?>
				<div class="copyright"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_footer_copyright', 'option' ) ) ); ?></div>
			<?php endif; ?>

	  </div>
	  <!-- o-container -->

	</footer>
	<!-- c-footer -->

<?php endif; ?>

<?php if ( have_rows( 'x5_general_popups_rows', 'option' ) ): ?>

	<?php while( have_rows( 'x5_general_popups_rows', 'option' ) ) : the_row(); ?>

		<?php if ( get_sub_field( 'x5_general_popups_rows_id', 'option' ) ): ?>

			<div id="<?php echo esc_attr( get_sub_field ( 'x5_general_popups_rows_id', 'option' ) ); ?>" class="popup-content mfp-hide">

				<?php if ( get_sub_field( 'x5_general_popups_rows_desc', 'option' ) ): ?>
					<?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_general_popups_rows_desc', 'option' ) ) ); ?>
				<?php endif; ?>

			</div>

		<?php endif; ?>

	<?php endwhile; ?>

<?php endif; ?>

<?php
	// do not remove
	wp_footer();
?>

</body>
</html>
